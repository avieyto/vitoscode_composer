<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/21/2019
 * Time: 2:02 PM
 */

namespace Vitoscode\Tests\Paypal\v2;


use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Vitoscode\Commons\BaseRestApiClient;
use Vitoscode\Commons\Event\BeforeSendApiEvent;
use Vitoscode\Commons\Event\Events;
use Vitoscode\PaypalApi\IPaypalApiEnvironment;
use Vitoscode\PaypalApi\v2\IPaypalApiClient;
use Vitoscode\PaypalApi\v2\Model\Amount;
use Vitoscode\PaypalApi\v2\Model\ApplicationContext;
use Vitoscode\PaypalApi\v2\Model\Payer;
use Vitoscode\PaypalApi\v2\Model\PaymentMethod;
use Vitoscode\PaypalApi\v2\Model\PurchaseUnitRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CreateOrderRequest;
use Vitoscode\PaypalApi\v2\Model\Response\CreateOrderResponse;
use Vitoscode\PaypalApi\v2\TokenStorage\MemoryTokenStorageHandler;
use Vitoscode\PaypalApi\v2\PaypalApiClient;
use Vitoscode\PaypalApi\v2\PaypalApiEnvironment;

class PaypalApiClientTest extends TestCase
{
    /**
     * @var IPaypalApiEnvironment $paypalEnvironment
     */
    protected $paypalEnvironment;

    /**
     * @var IPaypalApiClient $paypalApiClient
     */
    protected $paypalApiClient;

    /**
     * PaypalApiClientTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     * @throws \Vitoscode\PaypalApi\v2\Exception\PaypalException
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->paypalEnvironment = new PaypalApiEnvironment([
            'environmentActive' => 'dev',
            'environments' => [
                'dev' => [
                    'urlBase' => 'https://api.sandbox.paypal.com',
                    'apiKey' => 'AWm2UxB7DHaWMT9WimzM5QqdyR-qRL8DD3RI4tqNWZFt7Zh4SYH-UabwcWAp',
                    'apiSecret' => 'EBBm7BAJ9Z8V7k8oGsBjKPAIEttr5v3esHyUnFT-VXjOiJtpxsiCKBel250v',
                    'webExperienceId' => ''
                ],
                'prod' => [
                    'urlBase' => 'https://api.paypal.com',
                    'apiKey' => '',
                    'apiSecret' => '',
                    'webExperienceId' => ''
                ]
            ]
        ]);
        $tokenStorageHandler = new MemoryTokenStorageHandler();
        $this->paypalApiClient = new PaypalApiClient('Paypal', $this->paypalEnvironment, $tokenStorageHandler);
        /*$eventDispatcher = new EventDispatcher();
        $eventDispatcher->addListener(Events::API_BEFORE_SEND, function(BeforeSendApiEvent $event) {
            echo 'Before Execute Call Method = ' .$event->methodName;
        });
        $this->paypalApiClient->setDispatcher($eventDispatcher);*/
    }

    /**
     * @throws \Vitoscode\PaypalApi\v2\Exception\PaypalException
     * @throws \Exception
     */
    public function testGetAccessToken()
    {
        $token = $this->paypalApiClient->getAccessToken();
        $this->assertNotNull($token, 'Token is null');
        $this->assertNotNull($token->access_token, 'Access token is null');
        $this->assertNotTrue($token->isExpired(), 'token is expired');
    }

    public function testCreateOrder()
    {
        $request = new CreateOrderRequest();
        $request->intent = 'CAPTURE';
        $request->purchase_units = [];
        $purchaseUnit = new PurchaseUnitRequest();
        $purchaseUnit->amount = new Amount();
        $purchaseUnit->amount->value = 20;
        $purchaseUnit->amount->currency_code = 'USD';
        $purchaseUnit->description = 'Recharge for 20CUC';
        $purchaseUnit->soft_descriptor = 'Recharge online';
        $request->purchase_units[] = $purchaseUnit;
        $request->application_context = new ApplicationContext();
        $request->application_context->brand_name = 'Llamartocuba';
        $request->application_context->cancel_url = 'http://localhost/llamartocuba/cancel_recharge.php';
        $request->application_context->landing_page = ApplicationContext::LANDING_PAGE_LOGIN;
        $request->application_context->locale = 'es-ES';
        $request->application_context->payment_method = new PaymentMethod();
        $request->application_context->payment_method->payee_preferred = PaymentMethod::PAYEE_PREFERRED_IMMEDIATE_PAYMENT_REQUIRED;
        $request->application_context->return_url = 'http://localhost/llamartocuba/approve_recharge.php';
        $request->application_context->shipping_preference = ApplicationContext::SHIPPING_ADDRESS_PREFERENCE_NO_SHIPPING;
        $request->application_context->user_action = ApplicationContext::USER_ACTION_CONTINUE;
        $order = $this->paypalApiClient->createOrder($request);
        $token = $this->paypalApiClient->getLastAccessTokenUsed();
        $this->assertNotNull($order, 'order is null');
        $this->assertNotNull($token, 'token is null');
        $orderClass = $order instanceof CreateOrderResponse;
        $this->assertTrue($orderClass, 'order is not an order response class');
        /*foreach ($order->links as $hateoasLink) {
            echo $hateoasLink->rel . ' ' . $hateoasLink->href. "\n";
        }*/
    }
}
