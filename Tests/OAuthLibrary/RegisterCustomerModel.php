<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 2:05 PM
 */

namespace Vitoscode\Tests\OAuthLibrary;


use Vitoscode\OAuthLibrary\IRegisterCustomerModel;

class RegisterCustomerModel implements IRegisterCustomerModel
{
    protected $accountType;
    protected $password;
    protected $username;

    public function __construct($username, $password, $accountType)
    {
        $this->username = $username;
        $this->password = $password;
        $this->accountType = $accountType;
    }

    function getAccountType()
    {
        return $this->accountType;
    }

    function getPassword()
    {
        return $this->password;
    }

    function getUsername()
    {
        return $this->username;
    }
}