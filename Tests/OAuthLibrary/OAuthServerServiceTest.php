<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/13/2019
 * Time: 2:26 PM
 */

namespace Vitoscode\Tests\OAuthLibrary;

use Exception;
use PHPUnit\Framework\TestCase;
use Vitoscode\OAuthLibrary\Exception\OAuthException;
use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\OAuthClient;
use Vitoscode\OAuthLibrary\Model\OAuthSignature;
use Vitoscode\OAuthLibrary\Model\OAuthToken;
use Vitoscode\OAuthLibrary\OAuthServerService;
use Vitoscode\OAuthLibrary\Signature\Encoder\Md5SignatureEncoder;
use Vitoscode\OAuthLibrary\Signature\SignatureValidator;
use Vitoscode\Util\IdGenerator\UniqueGenerator;

final class OAuthServerServiceTest extends TestCase
{
    /**
     * @var MockOAuthDbRepository $dbRepository
     */
    protected $dbRepository;

    /**
     * @var Md5SignatureEncoder $signatureEncoder
     */
    protected $signatureEncoder;

    /**
     * @var SignatureValidator $signatureValidator
     */
    protected $signatureValidator;

    /**
     * @var UniqueGenerator $uidGenerator
     */
    protected $uidGenerator;

    /**
     * @var OAuthServerService $oauthServerService
     */
    protected $oauthServerService;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->dbRepository = new MockOAuthDbRepository();
        $this->signatureEncoder = new Md5SignatureEncoder();
        $this->signatureValidator = new SignatureValidator($this->signatureEncoder);
        $this->uidGenerator = new UniqueGenerator();
        $this->oauthServerService = new OAuthServerService($this->dbRepository, $this->signatureValidator, $this->uidGenerator);
    }

    /**
     * @throws Exception
     */
    public function testConstructor()
    {
        $this->assertNotNull($this->oauthServerService, "OAuthServerService is null");
    }

    /**
     * @throws OAuthException
     * @throws Exception
     */
    public function testGenerateClient()
    {
        $client = $this->oauthServerService->GenerateClient('clientName', OAuthClient::WEB_CLIENT_TYPE, 'http://localhost');
        $this->assertEquals($client->getClientName(), 'clientName');
        $this->assertNotNull($client->getClientSecret());
        $this->assertEquals($client->getClientType(), OAuthClient::WEB_CLIENT_TYPE);
        $this->assertNotNull($client->getClientId());
    }

    /**
     * @throws OAuthException
     * @throws Exception
     */
    public function testRegisterCustomer()
    {
        $registerCustomerModel = new RegisterCustomerModel('avieyto', 'mongueria', IOAuthAccount::ACCOUNT_TYPE_ADMIN);
        $signature = $this->createDefaultSignature();
        $account = $this->oauthServerService->RegisterCustomer($registerCustomerModel, $signature);
        $this->assertNotNull($account);
        $this->assertEquals(md5('mongueria'), $account->getAccountSecret());
    }

    /**
     * @throws OAuthException
     * @throws Exception
     */
    public function testGetTemporaryToken()
    {
        $signature = $this->createDefaultSignature();
        $temporaryToken = $this->oauthServerService->GetTemporaryToken($signature);
        $this->assertNotNull($temporaryToken);
        $this->assertNotNull($temporaryToken->getToken());
        $this->assertNotNull($temporaryToken->getTokenSecret());
        $this->assertEquals(OAuthToken::TOKEN_TYPE_TEMPORARY, $temporaryToken->getTokenType());
        $this->assertNotNull($temporaryToken->getRefreshToken());
    }

    /**
     * @throws OAuthException
     * @throws Exception
     */
    public function testGetAccessToken()
    {
        $userId = $this->uidGenerator->generateId();
        $signature = $this->createAccessTokenSignature($userId);
        $accessToken = $this->oauthServerService->GetAccessToken($userId, $signature);
        $this->assertNotNull($accessToken);
        $this->assertNotNull($accessToken->getToken());
        $this->assertNotNull($accessToken->getTokenSecret());
        $this->assertEquals(OAuthToken::TOKEN_TYPE_ACCESS, $accessToken->getTokenType());
        $this->assertNotNull($accessToken->getRefreshToken());
    }

    /**
     * @throws OAuthException
     * @throws Exception
     */
    public function testRefreshToken()
    {
        $refreshTokenId = $this->uidGenerator->generateId();
        $signature = $this->createVerifyTokenSignature($refreshTokenId);
        $accessToken = $this->oauthServerService->RefreshToken($refreshTokenId, $signature);
        $this->assertNotNull($accessToken);
        $this->assertNotNull($accessToken->getToken());
        $this->assertNotNull($accessToken->getTokenSecret());
        $this->assertEquals(OAuthToken::TOKEN_TYPE_ACCESS, $accessToken->getTokenType());
        $this->assertNotNull($accessToken->getRefreshToken());
    }

    /**
     * @throws OAuthException
     * @throws Exception
     */
    public function testVerifyToken()
    {
        $tokenId = $this->uidGenerator->generateId();
        $signature = $this->createVerifyTokenSignature($tokenId);
        $token = $this->oauthServerService->CheckTokenSignature($signature);
        $valid = ($token instanceof OAuthToken && $token->getToken() == $tokenId);
        $this->assertTrue($valid);
    }

    /**
     * @return string|OAuthSignature
     * @throws OAuthException
     * @throws Exception
     */
    private function createDefaultSignature()
    {
        $datetime = new \DateTime('now');
        $nonce = $this->uidGenerator->generateId();
        $signature = $this->signatureEncoder
            ->setNonce($nonce)
            ->setDateTime($datetime)
            ->setClientSecret('clientSecret')
            ->encode();
        $signature = new OAuthSignature($this->uidGenerator->generateId(), $nonce, $datetime->format('Y-m-d H:i:s'), $signature);
        return $signature;
    }

    /**
     * @param $userId
     * @return string|OAuthSignature
     * @throws OAuthException
     * @throws Exception
     */
    private function createAccessTokenSignature($userId)
    {
        $datetime = new \DateTime('now');
        $nonce = $this->uidGenerator->generateId();
        $accountSecret = md5($userId);
        $signature = $this->signatureEncoder
            ->setNonce($nonce)
            ->setDateTime($datetime)
            ->setClientSecret('clientSecret')
            ->setAccountSecret($accountSecret)
            ->encode();
        $signature = new OAuthSignature($this->uidGenerator->generateId(), $nonce, $datetime->format('Y-m-d H:i:s'), $signature);
        return $signature;
    }

    /**
     * @param $tokenId
     * @return string|OAuthSignature
     * @throws OAuthException
     * @throws Exception
     */
    private function createVerifyTokenSignature($tokenId)
    {
        $datetime = new \DateTime('now');
        $nonce = $this->uidGenerator->generateId();
        $tokenSecret = md5($tokenId);
        $signature = $this->signatureEncoder
            ->setNonce($nonce)
            ->setDateTime($datetime)
            ->setClientSecret('clientSecret')
            ->setTokenSecret($tokenSecret)
            ->encode();
        $signature = new OAuthSignature($this->uidGenerator->generateId(), $nonce, $datetime->format('Y-m-d H:i:s'), $signature, $tokenId);
        return $signature;
    }
}
