<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/16/2019
 * Time: 4:08 PM
 */

namespace Vitoscode\Tests\OAuthLibrary;


use Vitoscode\OAuthLibrary\DbRepository\IDbClientRepository;
use Vitoscode\OAuthLibrary\DbRepository\IDbNonceRepository;
use Vitoscode\OAuthLibrary\DbRepository\IDbRepository;
use Vitoscode\OAuthLibrary\DbRepository\IDbTokenRepository;
use Vitoscode\OAuthLibrary\DbRepository\IDbUserAccountRepository;
use Vitoscode\OAuthLibrary\IRegisterCustomerModel;
use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;
use Vitoscode\OAuthLibrary\Model\OAuthAccount;
use Vitoscode\OAuthLibrary\Model\OAuthClient;
use Vitoscode\OAuthLibrary\Model\OAuthToken;
use Vitoscode\Util\DateTime\DateTimeHelper;
use Vitoscode\Util\IdGenerator\UniqueGenerator;

class MockOAuthDbRepository implements IDbRepository, IDbClientRepository, IDbTokenRepository, IDbNonceRepository, IDbUserAccountRepository
{
    public function getClientRepository()
    {
        return $this;
    }

    public function getNonceRepository()
    {
        return $this;
    }

    public function getTokenRepository()
    {
        return $this;
    }

    public function getUserAccountRepository()
    {
        return $this;
    }

    /**
     * @param $clientId
     * @return IOAuthClient
     */
    public function getClientById($clientId)
    {
        $client = new OAuthClient($clientId, "clientName", "clientSecret", OAuthClient::WEB_CLIENT_TYPE);
        return $client;
    }

    /**
     * @param IOAuthClient $oauthClient
     * @return IOAuthClient
     */
    public function storeClient(IOAuthClient $oauthClient)
    {
        return $oauthClient;
    }

    /**
     * @param string $nonce
     * @return bool
     */
    public function existNonce($nonce)
    {
        return false;
    }

    /**
     * @param $nonce
     * @param \DateTime $dateTime
     * @return mixed
     */
    public function addNonce($nonce, $dateTime)
    {
        // TODO: Implement addNonce() method.
    }

    /**
     * @param IOAuthToken $token
     * @param IOAuthClient $client
     * @param IOAuthAccount $account
     * @return mixed
     */
    public function storeToken(IOAuthToken $token, IOAuthClient $client, IOAuthAccount $account = null)
    {
        return $token;
    }

    public function updateToken(IOAuthToken $token, IOAuthClient $client, IOAuthAccount $account = null)
    {
        return $token;
    }

    /**
     * @param string $tokenId
     * @return IOAuthToken
     * @throws \Exception
     */
    public function findToken($tokenId)
    {
        return new OAuthToken($tokenId, md5($tokenId), OAuthToken::TOKEN_TYPE_ACCESS, 3600, $tokenId, DateTimeHelper::getDateTime());
    }

    /**
     * @param IRegisterCustomerModel $customerModel
     * @param IOAuthClient $client
     * @return IOAuthAccount
     */
    public function registerUserAccount(IRegisterCustomerModel $customerModel, IOAuthClient $client)
    {
        $account = new OAuthAccount($customerModel->getUsername(), md5($customerModel->getPassword()));
        return $account;
    }

    /**
     * @param string $userId
     * @return IOAuthAccount|null
     */
    public function getUserAccount($userId)
    {
        $account = new OAuthAccount($userId, md5($userId));
        return $account;
    }

    /**
     * @param IOAuthToken $token
     * @return IOAuthAccount|null
     */
    public function getUserAccountByToken(IOAuthToken $token)
    {
        $generationIdService = new UniqueGenerator();
        return $this->getUserAccount($generationIdService->generateId());
    }

    /**
     * @param IOAuthToken $token
     * @return IOAuthClient
     */
    public function getClientByToken(IOAuthToken $token)
    {
        $generationIdService = new UniqueGenerator();
        return $this->getClientById($generationIdService->generateId());
    }
}
