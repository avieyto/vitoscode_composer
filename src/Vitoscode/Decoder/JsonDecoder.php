<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/18/2018
 * Time: 10:15 AM
 */

namespace Vitoscode\Decoder;


class JsonDecoder
{
    /**
     * @param string $className
     * @param \stdClass $jsonObject
     * @return null|object
     */
    public static function JsonObject2Object($className, $jsonObject)
    {
        try
        {
            $reflectionClass = new \ReflectionClass($className);
            $object = $reflectionClass->newInstance();
            $properties = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC);
            /**
             * @var $property \ReflectionProperty
             */
            foreach($properties as $property) {
                $name = $property->getName();
                if (isset($jsonObject->{$name})) {
                    $value = $jsonObject->{$name};
                    if (is_object($value)) {
                        $type = self::extractType($property);
                        if ($type == 'stdClass')
                            $property->setValue($object, $value);
                        else
                        {
                            $insideObject = self::JsonObject2Object($type, $value);
                            $property->setValue($object, $insideObject);
                        }
                    }
                    elseif (is_array($value) && $value != null) {
                        $type = self::extractType($property, true);
                        $array = array();
                        foreach ($value as $item) {
                            if (is_object($item))
                            {
                                if ($type == 'stdClass')
                                    $array[] = $item;
                                else {
                                    $insideObject = self::JsonObject2Object($type, $item);
                                    $array[] = $insideObject;
                                }
                            }
                            else
                                $array[] = $item;
                        }
                        $property->setValue($object, $array);
                    }
                    else
                        $property->setValue($object, $value);
                }
            }
            return $object;
        }
        catch(\Exception $ex) {
            echo $ex->getMessage();
            return null;
        }
    }

    /**
     * @param \ReflectionProperty $property
     * @param bool $isArray
     * @return bool|mixed|string
     */
    private static function extractType(\ReflectionProperty $property, $isArray = false)
    {
        try {
            $comment = $property->getDocComment();
            $pieces = explode("\n", $comment);
            if (isset($pieces[1])) {
                $pos1 = strpos($pieces[1], '@var ');
                $pos2 = strpos($pieces[1], '$');
                $type = substr($pieces[1], $pos1 + 5, ($pos2 - $pos1 - 5));
                $type = trim($type);
                if ($isArray)
                    $type = str_replace(array('[', ']'), '', $type);
                if (class_exists($type)) {
                    return $type;
                }
                else
                    throw new \Exception('Class name '.$type.'does not exist');
            }
        }
        catch (\Exception $ex) {
            //var_dump($ex); die();
            return 'stdClass';
        }
        return 'stdClass';
    }
}