<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 8/20/2019
 * Time: 12:04 PM
 */

namespace Vitoscode\Decoder;


class ArrayJsonDecoder
{
    /**
     * @param string $className
     * @param array $arrayJsonObjects
     * @return array
     */
    public static function ArrayJsonObject2Object($className, array $arrayJsonObjects)
    {
        $responseArrayNew = [];
        foreach($arrayJsonObjects as $item) {
            $responseArrayNew[] = JsonDecoder::JsonObject2Object($className, $item);
        }
        return $responseArrayNew;
    }
}