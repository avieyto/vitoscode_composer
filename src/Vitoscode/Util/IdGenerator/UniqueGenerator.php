<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/13/2018
 * Time: 10:18 AM
 */

namespace Vitoscode\Util\IdGenerator;


class UniqueGenerator implements IUniqueGenerator
{
    /**
     * UniqueGenerator constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return string
     */
    public function generateId()
    {
        $id = '';
        if (function_exists('com_create_guid'))
        {
            $guid = com_create_guid();
            $id = substr($guid, 1, strlen($guid) - 2);
        }
        else
        {
            mt_srand((double)microtime()*10000); //optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $id = //chr(123)// "{"
                //.
                substr($charid, 0, 8)
                .$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
            //.chr(125);// "}"
        }

        return str_replace('-', '', $id);
    }

    /**
     * @return string
     */
    public function generateSecret()
    {
        $return = "";
        $max = rand(30,35);
        for($i=0;$i<$max;$i++)
            $return .= chr(rand(65,122));
        $return = str_replace(array('=','[','\\',']','^','_','`'),array('','','','',''), $return);
        return str_replace("=", "", base64_encode($return));
    }
}