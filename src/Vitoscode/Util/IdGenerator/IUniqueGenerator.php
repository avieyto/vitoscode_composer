<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 12/13/2018
 * Time: 9:20 AM
 */

namespace Vitoscode\Util\IdGenerator;


interface IUniqueGenerator
{
    /**
     * @return string
     */
    function generateId();

    /**
     * @return string
     */
    function generateSecret();
}