<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/15/2019
 * Time: 10:24 AM
 */

namespace Vitoscode\Util\DateTime;


class DateTimeHelper
{
    /**
     * @return \DateTime
     * @throws \Exception
     */
    public static function getDateTime()
    {
        $datetime = new \DateTime();
        $datetime->setTimezone(new \DateTimeZone('UTC'));
        return $datetime;
    }

    /**
     * Return time in format hashtime to used in first data api.
     * @param \DateTime $dateTime
     * @return string
     */
    public static function getHashTime(\DateTime $dateTime)
    {
        return $dateTime->format('Y-m-d').'T'.$dateTime->format('H:i:s').'Z';
    }

    public static function somethingNew()
    {
        return 'somethingNew';
    }
}
