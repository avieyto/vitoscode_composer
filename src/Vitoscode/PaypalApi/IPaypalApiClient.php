<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/26/2019
 * Time: 2:52 PM
 */

namespace Vitoscode\PaypalApi;


use Vitoscode\Commons\IApiClient;
use Vitoscode\PaypalApi\Exception\IPaypalException;
use Vitoscode\PaypalApi\Model\PaypalToken;
use Vitoscode\PaypalApi\Request\CapturePaymentRequest;
use Vitoscode\PaypalApi\Request\CreatePaymentRequest;
use Vitoscode\PaypalApi\Request\CreateWebExperienceProfileRequest;
use Vitoscode\PaypalApi\Request\ExecutePaymentRequest;
use Vitoscode\PaypalApi\Response\CapturePaymentResponse;
use Vitoscode\PaypalApi\Response\CreatePaymentResponse;
use Vitoscode\PaypalApi\Response\CreateWebExperienceProfileResponse;
use Vitoscode\PaypalApi\Response\ExecutePaymentResponse;
use Vitoscode\PaypalApi\Response\PaypalError;
use Vitoscode\PaypalApi\Response\VoidPaymentResponse;
use Vitoscode\PaypalApi\TokenStorage\ITokenStorageHandler;

interface IPaypalApiClient extends IApiClient
{
    /**
     * @param ITokenStorageHandler $handler
     * @return mixed
     */
    function setTokenStorageHandler(ITokenStorageHandler $handler);

    /**
     * @return ITokenStorageHandler
     */
    function getTokenStorageHandler();

    /**
     * Create web experience on paypal
     * @param CreateWebExperienceProfileRequest $createWebExperienceProfile
     * @return CreateWebExperienceProfileResponse|PaypalError
     * @throws IPaypalException
     */
    function createPaymentWebExperience(CreateWebExperienceProfileRequest $createWebExperienceProfile);

    /**
     * Create a paypal access access_token
     * @param string $apiKey
     * @param string $apiSecret
     * @return PaypalToken|PaypalError
     * @throws IPaypalException
     */
    function createAccessToken($apiKey, $apiSecret);

    /**
     * Check if there is an access access_token created and no expires_in, otherwise try to create it.
     * @return string | boolean
     * @throws IPaypalException
     */
    function getAccessToken();

    /**
     * Create a paypal payment
     * @param CreatePaymentRequest $createPaymentRequest
     * @return CreatePaymentResponse|PaypalError
     * @throws IPaypalException
     */
    function createPayment(CreatePaymentRequest $createPaymentRequest);

    /**
     * Execute a payment created before
     * @param ExecutePaymentRequest $payment
     * @param string $paymentId
     * @param string $token
     * @return ExecutePaymentResponse|PaypalError
     * @throws IPaypalException
     */
    function executePayment(ExecutePaymentRequest $payment, $paymentId, $token);

    /**
     * Capture an authorization payment created and executed before
     * @param CapturePaymentRequest $capturePayment
     * @param string $authorizationId
     * @param string $token
     * @return CapturePaymentResponse
     * @throws IPaypalException
     */
    function capturePayment(CapturePaymentRequest $capturePayment, $authorizationId, $token);

    /**
     * Void a payment created and executed before
     * @param string $authorizationId
     * @param $token
     * @return VoidPaymentResponse|PaypalError
     * @throws IPaypalException
     */
    function voidPayment($authorizationId, $token);

    /**
     * @return mixed
     */
    function refundPayment();
}