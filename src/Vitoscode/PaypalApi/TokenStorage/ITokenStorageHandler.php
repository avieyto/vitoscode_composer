<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 2:22 PM
 */

namespace Vitoscode\PaypalApi\TokenStorage;


use Vitoscode\PaypalApi\Model\PaypalToken;

interface ITokenStorageHandler
{
    /**
     * Store a access_token in storage
     * @param PaypalToken $token
     * @return mixed
     */
    function storeToken(PaypalToken $token);

    /**
     * Retrieve access_token from storage
     * @return PaypalToken
     */
    function retrieveToken();
}