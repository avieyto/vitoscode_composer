<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 2:25 PM
 */

namespace Vitoscode\PaypalApi\TokenStorage;


use Vitoscode\PaypalApi\Model\PaypalToken;

class MemoryTokenStorageHandler implements ITokenStorageHandler
{
    /**
     * @var PaypalToken $token
     */
    protected $token;

    /**
     * @inheritdoc
     */
    public function storeToken(PaypalToken $token)
    {
        $this->token = $token;
    }

    /**
     * @inheritdoc
     */
    public function retrieveToken()
    {
        return $this->token;
    }
}