<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/22/2018
 * Time: 10:21 PM
 */

namespace Vitoscode\PaypalApi\Response;


class VoidPaymentResponse extends PaypalAbstractResponse
{
    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalAmount $amount
     */
    public $amount;

    /**
     * @var string $payment_mode
     */
    public $payment_mode;

    /**
     * @var string $reason_code
     */
    public $reason_code;

    /**
     * @var string $protection_eligibility
     */
    public $protection_eligibility;

    /**
     * @var string $protection_eligibility_type
     */
    public $protection_eligibility_type;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalFMFDetails $fmf_details
     */
    public $fmf_details;

    /**
     * @var string $parent_payment
     */
    public $parent_payment;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalProcessorResponse $processor_response
     */
    public $processor_response;

    /**
     * @var string $valid_until
     */
    public $valid_until;

    /**
     * @var string $reference_id
     */
    public $reference_id;

    /**
     * @var string $receipt_id
     */
    public $receipt_id;
}