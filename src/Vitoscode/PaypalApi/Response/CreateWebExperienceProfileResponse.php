<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 8:58 AM
 */

namespace Vitoscode\PaypalApi\Response;


use Vitoscode\PaypalApi\Request\CreateWebExperienceProfileRequest;

class CreateWebExperienceProfileResponse extends CreateWebExperienceProfileRequest
{
    /**
     * @var string $id
     */
    public $id;
}