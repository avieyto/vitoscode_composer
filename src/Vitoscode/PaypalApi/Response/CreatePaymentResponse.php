<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/18/2018
 * Time: 9:59 AM
 */

namespace Vitoscode\PaypalApi\Response;


class CreatePaymentResponse extends PaypalAbstractResponse
{
    /**
     * @var string $intent
     */
    public $intent;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalPayer $payer
     */
    public $payer;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalTransaction[] $transactions
     */
    public $transactions;

    /**
     * @var string $experience_profile_id
     */
    public $experience_profile_id;

    /**
     * @var string $note_to_payer
     */
    public $note_to_payer;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalRedirectUrls $redirect_urls
     */
    public $redirect_urls;

    /**
     * @var string $failure_reason
     */
    public $failure_reason;

    /**
     * @var string $tokenUsed
     */
    public $tokenUsed;
}