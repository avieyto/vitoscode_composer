<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 3/8/2018
 * Time: 12:42 PM
 */

namespace Vitoscode\PaypalApi\Response;


class PaypalError
{
    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $details
     */
    public $details;

    /**
     * @var string $message
     */
    public $message;

    /**
     * @var string $information_link
     */
    public $information_link;

    /**
     * @var string $debug_id
     */
    public $debug_id;
}