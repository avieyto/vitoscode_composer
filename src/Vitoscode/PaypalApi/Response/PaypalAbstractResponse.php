<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/18/2018
 * Time: 9:59 AM
 */

namespace Vitoscode\PaypalApi\Response;


class PaypalAbstractResponse
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var string $state
     */
    public $state;

    /**
     * @var string $create_time
     */
    public $create_time;

    /**
     * @var string $update_time
     */
    public $update_time;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalLink[] $links
     */
    public $links;

    /**
     * @return null|string
     */
    public function getRedirectUrl()
    {
        if ($this->links && is_array($this->links)) {
            foreach ($this->links as $link)
                if ($link->rel == 'approval_url' && $link->method == 'REDIRECT')
                    return $link->href;
        }
        return null;
    }
}