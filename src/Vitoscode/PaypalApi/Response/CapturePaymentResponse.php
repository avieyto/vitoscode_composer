<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/23/2018
 * Time: 3:07 AM
 */

namespace Vitoscode\PaypalApi\Response;

class CapturePaymentResponse extends PaypalAbstractResponse
{
    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalAmount $amount
     */
    public $amount;

    /**
     * @var boolean $is_final_capture
     */
    public $is_final_capture;

    /**
     * @var string $reason_code
     */
    public $reason_code;

    /**
     * @var string $parent_payment
     */
    public $parent_payment;

    /**
     * @var string $invoice_number
     */
    public $invoice_number;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalTransactionFee $transaction_fee
     */
    public $transaction_fee;
}