<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/26/2019
 * Time: 10:30 AM
 */

namespace Vitoscode\PaypalApi;


interface IPaypalUrlManager
{
    /**
     * Return the url for create a payment
     * @return string
     */
    function getCreatePaymentUrl();

    /**
     * Return the url for execute a payment
     * @param string $paymentId
     * @return string
     */
    function getExecutePaymentUrl($paymentId);

    /**
     * Return the url for capture a payment
     * @param string $authorizationId
     * @return string
     */
    function getCapturePaymentUrl($authorizationId);

    /**
     * Return the url for void a payment
     * @param string $authorizationId
     * @return string
     */
    function getVoidPaymentUrl($authorizationId);

    /**
     * Return the url for get a payment
     * @param string $paymentId
     * @return string
     */
    function getGetPaymentUrl($paymentId);

    /**
     * Return the url for get a access_token
     * @return string
     */
    function getGetTokenUrl();

    /**
     * Return the url for create a payment web experience
     * @return string
     */
    function getCreateWebExperienceUrl();
}