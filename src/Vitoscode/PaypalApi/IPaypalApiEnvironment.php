<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 1:56 PM
 */

namespace Vitoscode\PaypalApi;


use Vitoscode\Commons\Environment\IApiEnvironment;

interface IPaypalApiEnvironment extends IApiEnvironment
{
    /**
     * @return string
     */
    function getBaseUrl();

    /**
     * @return string
     */
    function getApiKey();

    /**
     * @return string
     */
    function getApiSecret();

    /**
     * @return string
     */
    function getWebExperienceId();
}