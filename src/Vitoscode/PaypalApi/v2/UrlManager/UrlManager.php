<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 2:13 PM
 */

namespace Vitoscode\PaypalApi\v2\UrlManager;


use Vitoscode\PaypalApi\v2\Exception\PaypalException;

class UrlManager implements IUrlManager
{
    protected $urls = [
        'create-order' => '/v2/checkout/orders',
        'update-order' => '/v2/checkout/orders/{order_id}',
        'show-order' => '/v2/checkout/orders/{order_id}',
        'authorize-payment' => '/v2/checkout/orders/{order_id}/authorize',
        'capture-payment' => '/v2/checkout/orders/{order_id}/capture',
        'show-authorized' => '/v2/payments/authorizations/{authorization_id}',
        'capture-authorized' => '/v2/payment/authorizations/{authorization_id}/capture',
        'reauthorize-authorized' => '/v2/payment/authorizations/{authorization_id}/reauthorize',
        'void-authorized' => '/v2/payment/authorizations/{authorization_id}/void',
        'show-captured-payment' => '/v2/payments/captures/{capture_id}',
        'refund-captured-payment' => '/v2/payments/captures/{capture_id}/refund',
        'show-refunded-payment' => '/v2/payments/refunds/{refund_id}',
        'get-token' => '/v1/oauth2/token'
    ];

    /**
     * @var string $baseUrl
     */
    protected $baseUrl = 'https://api.sandbox.paypal.com';

    /**
     * UrlManager constructor.
     * @param array $urlSuffix
     */
    public function __construct($urlSuffix = [])
    {
        if (is_array($urlSuffix) && !empty($urlSuffix) && count($urlSuffix) > 0)
            $this->urls = $urlSuffix;
    }

    /**
     * @inheritdoc
     */
    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCreateOrderUrl()
    {
        return $this->getUrl('create-order');
    }

    /**
     * @inheritdoc
     */
    public function getUpdateOrderUrl($orderId)
    {
        $url = $this->getUrl('update-order');
        $url = str_replace('{order_id}', $orderId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getShowOrderDetailsUrl($orderId)
    {
        $url = $this->getUrl('show-order');
        $url = str_replace('{order_id}', $orderId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getAuthorizePaymentForOrderUrl($orderId)
    {
        $url = $this->getUrl('authorize-payment');
        $url = str_replace('{order_id}', $orderId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getCapturePaymentForOrderUrl($orderId)
    {
        $url = $this->getUrl('capture-payment');
        $url = str_replace('{order_id}', $orderId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getShowDetailsForAuthorizedPaymentUrl($authorizationId)
    {
        $url = $this->getUrl('show-authorized');
        $url = str_replace('{authorization_id}', $authorizationId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getCaptureAuthorizedPaymentUrl($authorizationId)
    {
        $url = $this->getUrl('capture-authorized');
        $url = str_replace('{authorization_id}', $authorizationId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getReauthorizeAuthorizedPaymentUrl($authorizationId)
    {
        $url = $this->getUrl('reauthorize-authorized');
        $url = str_replace('{authorization_id}', $authorizationId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getVoidAuthorizedPaymentUrl($authorizationId)
    {
        $url = $this->getUrl('void-authorized');
        $url = str_replace('{authorization_id}', $authorizationId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getShowCapturedPaymentDetailsUrl($captureId)
    {
        $url = $this->getUrl('show-captured-payment');
        $url = str_replace('{capture_id}', $captureId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getRefundCapturedPaymentUrl($captureId)
    {
        $url = $this->getUrl('refund-captured-payment');
        $url = str_replace('{capture_id}', $captureId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getShowRefundDetailsUrl($refundId)
    {
        $url = $this->getUrl('show-refunded-payment');
        $url = str_replace('{refund_id}', $refundId, $url);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function getAccessTokenUrl()
    {
        return $this->getUrl('get-token');
    }

    /**
     * @param $accessKey
     * @return string
     * @throws PaypalException
     */
    protected function getUrl($accessKey)
    {
        if (isset($this->urls[$accessKey]))
            return $this->baseUrl.$this->urls[$accessKey];
        throw new PaypalException('Invalid paypal api url', 400, null);
    }
}