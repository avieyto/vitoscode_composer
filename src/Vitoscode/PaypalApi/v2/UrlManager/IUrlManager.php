<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 3:00 PM
 */

namespace Vitoscode\PaypalApi\v2\UrlManager;


use Vitoscode\PaypalApi\v2\Exception\PaypalException;

interface IUrlManager
{
    /**
     * Set the base url of the Paypal Api
     * @param string $url
     * @return $this
     * @throws PaypalException
     */
    function setBaseUrl($url);

    /**
     * Return Create Order Paypal Api Url
     * @return string
     * @throws PaypalException
     */
    function getCreateOrderUrl();

    /**
     * Return Update Order Paypal Api Url
     * @param string $orderId
     * @return string
     * @throws PaypalException
     */
    function getUpdateOrderUrl($orderId);

    /**
     * Return the show order details Paypal Api Url
     * @param string $orderId
     * @return string
     * @throws PaypalException
     */
    function getShowOrderDetailsUrl($orderId);

    /**
     * Return authorize payment for order paypal api url
     * @param string $orderId
     * @return string
     * @throws PaypalException
     */
    function getAuthorizePaymentForOrderUrl($orderId);

    /**
     * Return capture payment for order paypal api url
     * @param string $orderId
     * @return string
     * @throws PaypalException
     */
    function getCapturePaymentForOrderUrl($orderId);

    /**
     * Return show details for authorized payment paypal api url
     * @param string $authorizationId
     * @return string
     * @throws PaypalException
     */
    function getShowDetailsForAuthorizedPaymentUrl($authorizationId);

    /**
     * Return capture authorized payment paypal api url
     * @param string $authorizationId
     * @return string
     * @throws PaypalException
     */
    function getCaptureAuthorizedPaymentUrl($authorizationId);

    /**
     * Return re authorize an authorized payment paypal api url
     * @param string $authorizationId
     * @return string
     * @throws PaypalException
     */
    function getReauthorizeAuthorizedPaymentUrl($authorizationId);

    /**
     * Return void an authorized payment paypal api url
     * @param string $authorizationId
     * @return string
     * @throws PaypalException
     */
    function getVoidAuthorizedPaymentUrl($authorizationId);

    /**
     * Return show captured payment details paypal api url
     * @param string $captureId
     * @return string
     * @throws PaypalException
     */
    function getShowCapturedPaymentDetailsUrl($captureId);

    /**
     * Return refund a captured payment paypal api url
     * @param string $captureId
     * @return string
     * @throws PaypalException
     */
    function getRefundCapturedPaymentUrl($captureId);

    /**
     * Return show a refund details paypal api url
     * @param string $refundId
     * @return string
     * @throws PaypalException
     */
    function getShowRefundDetailsUrl($refundId);

    /**
     * Return get access token paypal api url
     * @return string
     * @throws PaypalException
     */
    function getAccessTokenUrl();
}