<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 2:52 PM
 */

namespace Vitoscode\PaypalApi\v2;


use GuzzleHttp\Psr7\Response;
use Vitoscode\Commons\IApiClient;
use Vitoscode\PaypalApi\v2\Exception\PaypalException;
use Vitoscode\PaypalApi\v2\Headers\IPaypalHeadersConfig;
use Vitoscode\PaypalApi\v2\Model\Request\AuthorizePaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CaptureAuthorizedPaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CapturePaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CreateOrderRequest;
use Vitoscode\PaypalApi\v2\Model\Request\ReAuthorizeAuthorizedPaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\RefundCapturedPaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\UpdateOrderRequest;
use Vitoscode\PaypalApi\v2\Model\Response\AuthorizePaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\CaptureAuthorizedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\CapturePaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\CreateOrderResponse;
use Vitoscode\PaypalApi\v2\Model\Response\PaypalError;
use Vitoscode\PaypalApi\v2\Model\Response\ReAuthorizeAuthorizedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\RefundCapturedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowDetailsAuthorizedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowDetailsCapturedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowOrderDetailResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowRefundDetailsResponse;
use Vitoscode\PaypalApi\v2\Model\Token;
use Vitoscode\PaypalApi\v2\TokenStorage\ITokenStorageHandler;
use Vitoscode\PaypalApi\v2\UrlManager\IUrlManager;

interface IPaypalApiClient extends IApiClient
{
    /**
     * Perform a create order request to the paypal api
     * @param CreateOrderRequest $createOrderRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return CreateOrderResponse|PaypalError
     * @throws PaypalException
     */
    function createOrder(CreateOrderRequest $createOrderRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform an update order request to the paypal api
     * @param string $orderId
     * @param UpdateOrderRequest $updateOrderRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return string|PaypalError
     * @throws PaypalException
     */
    function updateOrder($orderId, UpdateOrderRequest $updateOrderRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Retrieve an order information through the paypal api
     * @param $orderId
     * @param IPaypalHeadersConfig $headersConfig
     * @return ShowOrderDetailResponse|PaypalError
     * @throws PaypalException
     */
    function showOrderDetails($orderId, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform an authorization payment through the paypal api
     * @param string $orderId
     * @param AuthorizePaymentRequest $authorizePaymentRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return AuthorizePaymentResponse|PaypalError
     * @throws PaypalException
     */
    function authorizePaymentForOrder($orderId, AuthorizePaymentRequest $authorizePaymentRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform a capture payment through the paypal api
     * @param string $orderId
     * @param CapturePaymentRequest $capturePaymentRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return CapturePaymentResponse|PaypalError
     * @throws PaypalException
     */
    function capturePaymentForOrder($orderId, CapturePaymentRequest $capturePaymentRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Retrieve an authorization details through the paypal api
     * @param string $authorizationId
     * @param IPaypalHeadersConfig $headersConfig
     * @return ShowDetailsAuthorizedPaymentResponse
     * @throws PaypalException
     */
    function showDetailsForAuthorizedPayment($authorizationId, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform a capture authorized payment through paypal api
     * @param string $authorizationId
     * @param CaptureAuthorizedPaymentRequest $captureAuthorizedPaymentRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return CaptureAuthorizedPaymentResponse|PaypalError
     * @throws PaypalException
     */
    function captureAuthorizedPayment($authorizationId, CaptureAuthorizedPaymentRequest $captureAuthorizedPaymentRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform a reauthorization to an authorized payment through paypal api
     * @param string $authorizationId
     * @param ReAuthorizeAuthorizedPaymentRequest $reAuthorizeAuthorizedPaymentRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return ReAuthorizeAuthorizedPaymentResponse
     * @throws PaypalException
     */
    function reauthorizedAuthorizedPayment($authorizationId, ReAuthorizeAuthorizedPaymentRequest $reAuthorizeAuthorizedPaymentRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform a void authorized payment through paypal api, return true or message with error
     * @param string $authorizationId
     * @param IPaypalHeadersConfig $headersConfig
     * @return string|PaypalError|Response
     * @throws PaypalException
     */
    function voidAuthorizedPayment($authorizationId, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Retrieve details of a captured payment through paypal api
     * @param string $captureId
     * @param IPaypalHeadersConfig $headersConfig
     * @return ShowDetailsCapturedPaymentResponse|PaypalError
     * @throws PaypalException
     */
    function showCapturedPaymentDetails($captureId, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Perform a refund captured payment through paypal api
     * @param string $captureId
     * @param RefundCapturedPaymentRequest $refundCapturedPaymentRequest
     * @param IPaypalHeadersConfig $headersConfig
     * @return RefundCapturedPaymentResponse|PaypalError
     * @throws PaypalException
     */
    function refundCapturedPayment($captureId, RefundCapturedPaymentRequest $refundCapturedPaymentRequest, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Retrieve details of a refund captured payment through paypal api
     * @param string $refundId
     * @param IPaypalHeadersConfig $headersConfig
     * @return ShowRefundDetailsResponse|PaypalError
     * @throws PaypalException
     */
    function showRefundDetails($refundId, IPaypalHeadersConfig $headersConfig = null);

    /**
     * Check if there is an access access_token created and no expires_in, otherwise try to create it.
     * @param IPaypalHeadersConfig $headersConfig
     * @return Token|null
     * @throws PaypalException
     */
    function getAccessToken(IPaypalHeadersConfig $headersConfig = null);

    /**
     * @param ITokenStorageHandler $handler
     * @return $this
     */
    function setTokenStorageHandler(ITokenStorageHandler $handler);

    /**
     * @return ITokenStorageHandler
     */
    function getTokenStorageHandler();

    /**
     * Get the last access token used.
     * @return Token
     */
    function getLastAccessTokenUsed();

    /**
     * Set a default $headersConfig to paypal, will be override it in each method if desire.
     * @param IPaypalHeadersConfig $headersConfig
     * @return $this
     */
    function setDefaultPaypalHeadersConfig(IPaypalHeadersConfig $headersConfig);

    /**
     * Set the uri manager for gets the urls of the paypal api
     * @param IUrlManager $urlManager
     * @return $this
     */
    function setPaypalUrlManager(IUrlManager $urlManager);

    /**
     * Get the uri manager for gets the urls of the paypal api
     * @return IUrlManager
     */
    function getPaypalUrlManager();
}