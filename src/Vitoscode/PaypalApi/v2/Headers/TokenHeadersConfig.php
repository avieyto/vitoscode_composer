<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/20/2019
 * Time: 2:15 PM
 */

namespace Vitoscode\PaypalApi\v2\Headers;


class TokenHeadersConfig implements IPaypalHeadersConfig
{

    /**
     * @return string
     */
    public function getPayPalPartnerAttributionId()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getPrefer()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return 'x-www-form-urlencoded';
    }

    /**
     * The server stores keys for 45 days.
     * @return string
     */
    public function getPaypalRequestId()
    {
        return null;
    }

    /**
     *
     * @return string
     */
    public function getPayPalClientMetadataId()
    {
        return null;
    }

    /**
     * An API-caller-provided JSON Web Token (JWT) assertion that identifies the merchant.
     * @return string
     */
    public function getPayPalAuthAssertion()
    {
        return null;
    }
}