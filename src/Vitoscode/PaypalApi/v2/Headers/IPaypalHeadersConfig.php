<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 11:36 AM
 */

namespace Vitoscode\PaypalApi\v2\Headers;


interface IPaypalHeadersConfig
{
    /**
     * @return string
     */
    function getPayPalPartnerAttributionId();

    /**
     * @return string
     */
    function getPrefer();

    /**
     * @return string
     */
    function getContentType();

    /**
     * The server stores keys for 45 days.
     * @return string
     */
    function getPaypalRequestId();

    /**
     *
     * @return string
     */
    function getPayPalClientMetadataId();

    /**
     * An API-caller-provided JSON Web Token (JWT) assertion that identifies the merchant.
     * @return string
     */
    function getPayPalAuthAssertion();
}