<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 2:22 PM
 */

namespace Vitoscode\PaypalApi\v2\TokenStorage;

use Vitoscode\PaypalApi\v2\Model\Token;

interface ITokenStorageHandler
{
    /**
     * Store a access_token in storage
     * @param Token $token
     * @return mixed
     */
    function storeToken(Token $token);

    /**
     * Retrieve access_token from storage
     * @return Token
     */
    function retrieveToken();
}