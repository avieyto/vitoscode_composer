<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 2:25 PM
 */

namespace Vitoscode\PaypalApi\v2\TokenStorage;


use Vitoscode\PaypalApi\Model\PaypalToken;
use Vitoscode\PaypalApi\v2\Model\Token;

class MemoryTokenStorageHandler implements ITokenStorageHandler
{
    /**
     * @var PaypalToken $token
     */
    protected $token;

    /**
     * @inheritdoc
     */
    public function storeToken(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @inheritdoc
     */
    public function retrieveToken()
    {
        return $this->token;
    }
}