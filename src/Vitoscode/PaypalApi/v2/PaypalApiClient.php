<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 1:13 PM
 */

namespace Vitoscode\PaypalApi\v2;

use GuzzleHttp\Psr7\Response;
use Vitoscode\Commons\BaseRestApiClient;
use Vitoscode\Commons\Environment\IApiEnvironment;
use Vitoscode\Commons\IApiClient;
use Vitoscode\Decoder\JsonDecoder;
use Vitoscode\Encoder\EncoderCleanJson;
use Vitoscode\PaypalApi\IPaypalApiEnvironment;
use Vitoscode\PaypalApi\v2\Exception\PaypalException;
use Vitoscode\PaypalApi\v2\Headers\IPaypalHeadersConfig;
use Vitoscode\PaypalApi\v2\Model\Request\AuthorizePaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CaptureAuthorizedPaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CapturePaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\CreateOrderRequest;
use Vitoscode\PaypalApi\v2\Model\Request\ReAuthorizeAuthorizedPaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\RefundCapturedPaymentRequest;
use Vitoscode\PaypalApi\v2\Model\Request\UpdateOrderRequest;
use Vitoscode\PaypalApi\v2\Model\Response\AuthorizePaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\CaptureAuthorizedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\CapturePaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\CreateOrderResponse;
use Vitoscode\PaypalApi\v2\Model\Response\PaypalError;
use Vitoscode\PaypalApi\v2\Model\Response\ReAuthorizeAuthorizedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\RefundCapturedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowDetailsAuthorizedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowDetailsCapturedPaymentResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowOrderDetailResponse;
use Vitoscode\PaypalApi\v2\Model\Response\ShowRefundDetailsResponse;
use Vitoscode\PaypalApi\v2\Model\Token;
use Vitoscode\PaypalApi\v2\TokenStorage\ITokenStorageHandler;
use Vitoscode\PaypalApi\v2\UrlManager\IUrlManager;
use Vitoscode\PaypalApi\v2\UrlManager\UrlManager;
use GuzzleHttp\Exception\GuzzleException;

class PaypalApiClient extends BaseRestApiClient implements IPaypalApiClient
{
    /**
     * @var ITokenStorageHandler $tokenStorage
     */
    protected $tokenStorage;

    /**
     * @var Token $_lastTokenUsed
     */
    protected $_lastTokenUsed;

    /**
     * @var IPaypalHeadersConfig $headersConfig
     */
    protected $headersConfig;

    /**
     * @var IUrlManager $paypalUrlManager
     */
    protected $paypalUrlManager;

    /**
     * IApiClient constructor.
     * @param $apiName
     * @param IApiEnvironment $apiEnvironments
     * @param IUrlManager $paypalUrlManager
     * @param ITokenStorageHandler $tokenStorageHandler
     * @param IPaypalHeadersConfig $headersConfig
     * @throws PaypalException
     */
    public function __construct($apiName, IApiEnvironment $apiEnvironments, ITokenStorageHandler $tokenStorageHandler = null, IUrlManager $paypalUrlManager = null, IPaypalHeadersConfig $headersConfig = null)
    {
        if (!($apiEnvironments instanceof IPaypalApiEnvironment))
            throw new PaypalException('Paypal Api environment must implement the interface \\Vitoscode\\PaypalApi\\IPaypalApiEnvironment');
        parent::__construct($apiName, $apiEnvironments);
        $this->setTokenStorageHandler($tokenStorageHandler);
        if ($headersConfig)
            $this->setDefaultPaypalHeadersConfig($headersConfig);
        if (!$paypalUrlManager) {
            $paypalUrlManager = new UrlManager();
            $paypalUrlManager->setBaseUrl($apiEnvironments->getBaseUrl());
        }
        $this->setPaypalUrlManager($paypalUrlManager);
    }

    /**
     * @inheritdoc
     */
    public function createOrder(CreateOrderRequest $createOrderRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getCreateOrderUrl();
        $data = EncoderCleanJson::encodeCleanToJson($createOrderRequest);
        //throw new \Exception($data);
        $guzzleResponse = $this->sendRequest("CREATE-ORDER", IApiClient::POST, $url, $data, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\CreateOrderResponse';
        /**
         * @var $response CreateOrderResponse
         */
        $response = $this->createAndValidResponse("CREATE-ORDER", $guzzleResponse, $responseClass, [200, 201]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function updateOrder($orderId, UpdateOrderRequest $updateOrderRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getUpdateOrderUrl($orderId);
        $data = json_encode($updateOrderRequest);
        $guzzleResponse = $this->sendRequest('UPDATE-ORDER', IApiClient::PATCH, $url, $data, $headersConfig, null);
        $responseClass = null;
        $response = $this->createAndValidResponse('UPDATE-ORDER', $guzzleResponse, $responseClass, [200, 204]);
        if ($response instanceof PaypalError)
            return $response;
        else if ($response instanceof Response)
            return $response->getReasonPhrase();
        else
            return $response;
    }

    /**
     * @inheritdoc
     */
    public function showOrderDetails($orderId, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getShowOrderDetailsUrl($orderId);
        $guzzleResponse = $this->sendRequest('SHOW-ORDER-DETAILS', IApiClient::GET, $url, null, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\ShowOrderDetailResponse';
        /**
         * @var $response ShowOrderDetailResponse
         */
        $response = $this->createAndValidResponse('SHOW-ORDER-DETAILS', $guzzleResponse, $responseClass, [200]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function authorizePaymentForOrder($orderId, AuthorizePaymentRequest $authorizePaymentRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getAuthorizePaymentForOrderUrl($orderId);
        $data = json_encode($authorizePaymentRequest);
        $guzzleResponse = $this->sendRequest('AUTHORIZE-PAYMENT', IApiClient::POST, $url, $data, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\AuthorizePaymentResponse';
        /**
         * @var $response AuthorizePaymentResponse
         */
        $response = $this->createAndValidResponse('AUTHORIZE-PAYMENT', $guzzleResponse, $responseClass, [200, 201]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function capturePaymentForOrder($orderId, CapturePaymentRequest $capturePaymentRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getCapturePaymentForOrderUrl($orderId);
        $data = json_encode($capturePaymentRequest);
        $guzzleResponse = $this->sendRequest('CAPTURE-PAYMENT-ORDER', IApiClient::POST, $url, $data, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\CapturePaymentResponse';
        /**
         * @var $response CapturePaymentResponse
         */
        $response = $this->createAndValidResponse('CAPTURE-PAYMENT-ORDER', $guzzleResponse, $responseClass, [200, 201]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function showDetailsForAuthorizedPayment($authorizationId, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getShowDetailsForAuthorizedPaymentUrl($authorizationId);
        $guzzleResponse = $this->sendRequest('SHOW-AUTHORIZED-PAYMENT-DETAILS', IApiClient::GET, $url, null, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\ShowDetailsAuthorizedPaymentResponse';
        /**
         * @var $respose ShowDetailsAuthorizedPaymentResponse
         */
        $response = $this->createAndValidResponse('SHOW-AUTHORIZED-PAYMENT-DETAILS', $guzzleResponse, $responseClass, [200]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function captureAuthorizedPayment($authorizationId, CaptureAuthorizedPaymentRequest $captureAuthorizedPaymentRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getShowDetailsForAuthorizedPaymentUrl($authorizationId);
        $data = json_encode($captureAuthorizedPaymentRequest);
        $guzzleResponse = $this->sendRequest('CAPTURE-AUTHORIZED-PAYMENT', IApiClient::POST, $url, $data, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\CaptureAuthorizedPaymentResponse';
        /**
         * @var $response CaptureAuthorizedPaymentResponse
         */
        $response = $this->createAndValidResponse('CAPTURE-AUTHORIZED-PAYMENT', $guzzleResponse, $responseClass, [200, 201]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function reauthorizedAuthorizedPayment($authorizationId, ReAuthorizeAuthorizedPaymentRequest $reAuthorizeAuthorizedPaymentRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getReauthorizeAuthorizedPaymentUrl($authorizationId);
        $data = json_encode($reAuthorizeAuthorizedPaymentRequest);
        $guzzleResponse = $this->sendRequest('REAUTHORIZE-AUTHORIZED-PAYMENT', IApiClient::POST, $url, $data, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\ReAuthorizeAuthorizedPaymentResponse';
        /**
         * @var $response ReAuthorizeAuthorizedPaymentResponse
         */
        $response = $this->createAndValidResponse('REAUTHORIZE-AUTHORIZED-PAYMENT', $guzzleResponse, $responseClass, [200, 201]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function voidAuthorizedPayment($authorizationId, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getVoidAuthorizedPaymentUrl($authorizationId);
        $guzzleResponse = $this->sendRequest('VOID-AUTHORIZED-PAYMENT', IApiClient::POST, $url, null, $headersConfig, null);
        $response = $this->createAndValidResponse('SHOW-AUTHORIZED-PAYMENT-DETAILS', $guzzleResponse, null, [200, 204]);
        if ($response instanceof PaypalError)
            return $response;
        else if ($response instanceof Response)
            return $response->getReasonPhrase();
        else
            return $response;
    }

    /**
     * @inheritdoc
     */
    public function showCapturedPaymentDetails($captureId, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getShowCapturedPaymentDetailsUrl($captureId);
        $guzzleResponse = $this->sendRequest('SHOW-CAPTURE-PAYMENT-DETAILS', IApiClient::GET, $url, null, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\ShowDetailsCapturedPaymentResponse';
        /**
         * @var $respose ShowDetailsCapturedPaymentResponse
         */
        $response = $this->createAndValidResponse('SHOW-CAPTURE-PAYMENT-DETAILS', $guzzleResponse, $responseClass, [200]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function refundCapturedPayment($captureId, RefundCapturedPaymentRequest $refundCapturedPaymentRequest, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getRefundCapturedPaymentUrl($captureId);
        $data = json_encode($refundCapturedPaymentRequest);
        $guzzleResponse = $this->sendRequest('REFUND-CAPTURED-PAYMENT', IApiClient::POST, $url, $data, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\RefundCapturedPaymentResponse';
        /**
         * @var $response RefundCapturedPaymentResponse
         */
        $response = $this->createAndValidResponse('REFUND-CAPTURED-PAYMENT', $guzzleResponse, $responseClass, [200, 201]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function showRefundDetails($refundId, IPaypalHeadersConfig $headersConfig = null)
    {
        $url = $this->paypalUrlManager->getShowRefundDetailsUrl($refundId);
        $guzzleResponse = $this->sendRequest('SHOW-REFUND-PAYMENT-DETAILS', IApiClient::GET, $url, null, $headersConfig, null);
        $responseClass = 'Vitoscode\PaypalApi\v2\Model\Response\ShowRefundDetailsResponse';
        /**
         * @var $response ShowRefundDetailsResponse
         */
        $response = $this->createAndValidResponse('SHOW-REFUND-PAYMENT-DETAILS', $guzzleResponse, $responseClass, [200]);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken(IPaypalHeadersConfig $headersConfig = null)
    {
        $tokenStorage = $this->getTokenStorageHandler();
        $token = $tokenStorage->retrieveToken();
        if ($token && !$token->isExpired()) {
            $this->_lastTokenUsed = $token;
            return $token;
        }
        else {
            try {
                /**
                 * @var $apiEnvironment IPaypalApiEnvironment
                 */
                $apiEnvironment = $this->apiEnvironments;
                $url = $this->paypalUrlManager->getAccessTokenUrl();
                $options = [
                    'headers' => [
                        'User-Agent' => 'PHP Vitoscode Paypal Api Client/2.0',
                        'Accept' => 'application/json',
                        'Accept-Languages' => 'en_US',
                        'Content-Type' => 'x-www-form-urlencoded'
                    ],
                    'verify' => false,
                    'body' => 'grant_type=client_credentials',
                    'auth' => [$apiEnvironment->getApiKey(), $apiEnvironment->getApiSecret()]
                ];
                $guzzleResponse = $this->request('GET-TOKEN', IApiClient::POST, $url, $options, null);
                $responseClass = 'Vitoscode\PaypalApi\v2\Model\Token';
                /**
                 * @var $response Token
                 */
                $response = $this->createAndValidResponse('GET-TOKEN', $guzzleResponse, $responseClass, [200]);
                if ($response instanceof Token) {
                    $tokenStorage->storeToken($response);
                    $this->_lastTokenUsed = $response;
                    return $response;
                }
                else
                    throw new PaypalException('An error occurred in the paypal token generation '.json_encode($response));
            }
            catch(GuzzleException $exception) {
                throw new PaypalException($exception->getMessage(), $exception->getCode(), $exception);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function setTokenStorageHandler(ITokenStorageHandler $handler)
    {
        $this->tokenStorage = $handler;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTokenStorageHandler()
    {
        return $this->tokenStorage;
    }

    /**
     * @inheritdoc
     */
    public function getLastAccessTokenUsed()
    {
        return $this->_lastTokenUsed;
    }

    /**
     * @inheritdoc
     */
    public function setDefaultPaypalHeadersConfig(IPaypalHeadersConfig $headersConfig)
    {
        $this->headersConfig = $headersConfig;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPaypalUrlManager(IUrlManager $urlManager)
    {
        $this->paypalUrlManager = $urlManager;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPaypalUrlManager()
    {
        return $this->paypalUrlManager;
    }

    /**
     * @param $methodName
     * @param $requestType
     * @param $url
     * @param string $dataPost
     * @param IPaypalHeadersConfig $additionalHeaders
     * @param null $context
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws PaypalException
     */
    protected function sendRequest($methodName, $requestType, $url, $dataPost = '', IPaypalHeadersConfig $additionalHeaders = null, $context = null)
    {
        try {
            $token = $this->getAccessToken($additionalHeaders);

            $options = [
                'headers' => $this->getHeaders($additionalHeaders),
                'verify' => false
            ];

            if ($token) {
                $options['headers']['Authorization'] = 'Bearer '.$token->access_token;
            }

            if ($requestType == IApiClient::POST || $requestType == IApiClient::PATCH) {
                if (!$dataPost || empty($dataPost))
                    throw new PaypalException('Missing request content for request call '.$methodName);
                $options['body'] = $dataPost;
            }

            /*if ($additionalHeaders && is_array($additionalHeaders)) {
                foreach ($additionalHeaders as $key => $value) {
                    if (isset($options[$key])) {
                        if (is_array($options[$key]) && is_array($value)) {
                            $options[$key] = array_merge($value, $options[$key]);
                        }
                    }
                    else {
                        $options[$key] = $value;
                    }
                }
            }*/
            return $this->request($methodName, $requestType, $url, $options, $context);
        }
        catch(GuzzleException $exception) {
            throw new PaypalException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param IPaypalHeadersConfig|null $additionalHeaders
     * @return array
     */
    protected function getHeaders(IPaypalHeadersConfig $additionalHeaders = null)
    {
        if (!$additionalHeaders)
            $additionalHeaders = $this->headersConfig;
        $headers = [
            'User-Agent' => 'PHP Vitoscode Paypal Api Client/2.0',
            'Accept' => 'application/json',
            'Accept-Languages' => 'en_US',
            'Content-Type' => 'application/json'
        ];
        if ($additionalHeaders) {
            if ($additionalHeaders->getContentType())
                $headers['Content-Type'] = $additionalHeaders->getContentType();
            if ($additionalHeaders->getPrefer())
                $headers['Prefer'] = $additionalHeaders->getPrefer();
            if ($additionalHeaders->getPayPalPartnerAttributionId())
                $headers['PayPal-Partner-Attribution-Id'] = $additionalHeaders->getPayPalPartnerAttributionId();
            if ($additionalHeaders->getPayPalAuthAssertion())
                $headers['PayPal-Auth-Assertion'] = $additionalHeaders->getPayPalAuthAssertion();
            if ($additionalHeaders->getPayPalClientMetadataId())
                $headers['PayPal-Client-Metadata-Id'] = $additionalHeaders->getPayPalClientMetadataId();
            if ($additionalHeaders->getPaypalRequestId())
                $headers['PayPal-Request-Id'] = $additionalHeaders->getPaypalRequestId();
        }
        return $headers;
    }

    /**
     * @param string $methodName
     * @param Response $response
     * @param string $className
     * @param array $validResponseCodes
     * @return object|PaypalError|Response
     * @throws PaypalException
     */
    protected function createAndValidResponse($methodName, Response $response, $className, $validResponseCodes)
    {
        if ($response) {
            if (!in_array($response->getStatusCode(), $validResponseCodes))
                throw new PaypalException('An error occurred requesting paypal invalid response code ' . $methodName . ' Reason: ' . $response->getReasonPhrase(), $response->getStatusCode(), null);
            if ($className) {
                if ($response->getBody()->getSize() <= 0)
                    throw new PaypalException('An error occurred requesting paypal blank response ' . $methodName, $response->getStatusCode(), null);
                $jsonObject = json_decode($response->getBody()->getContents());
                if (!$jsonObject)
                    throw new PaypalException('Invalid response returned by paypal on ' . $methodName. ' response: '.$response->getBody()->getContents(), 0, null);

                if (!isset($jsonObject->name) && !isset($jsonObject->details) && !isset($jsonObject->message)) {
                    $responseObject = JsonDecoder::JsonObject2Object($className, $jsonObject);
                }
                else {
                    $errorClass = 'Vitoscode\PaypalApi\v2\Model\Response\PaypalError';
                    $responseObject = JsonDecoder::JsonObject2Object($errorClass, $jsonObject);
                }
                return $responseObject;
            }
            else {
                return $response;
            }
        }
        else
            throw new PaypalException('An error occurred requesting paypal response object null ' . $methodName, 0, null);
    }
}