<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 2:37 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PatchRequest
{
    /**
     * @var string $op
     *
     * add, remove, replace, mode, copy, test
     */
    public $op;

    /**
     * @var string $path
     */
    public $path;

    /**
     * @var mixed $value
     */
    public $value;

    /**
     * @var string $from
     */
    public $from;
}