<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:09 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Address
{
    /**
     * @var string $address_line_1
     */
    public $address_line_1;

    /**
     * @var string $address_line_2
     *
     * The second line of the address. For example, suite or apartment number.
     */
    public $address_line_2;

    /**
     * @var string $admin_area_1
     *
     * The highest level sub-division in a country (State, County, Province, Prefecture, Kanton)
     * If is state of US, two letter like FL, NY.
     */
    public $admin_area_1;

    /**
     * @var string $admin_area_2
     *
     * A city, town, village
     */
    public $admin_area_2;

    /**
     * @var string $postal_code
     */
    public $postal_code;

    /**
     * @var string $country_code
     */
    public $country_code;
}