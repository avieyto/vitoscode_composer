<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:25 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Shipping
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\ShippingNameDetail $name
     */
    public $name;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Address $address
     */
    public $address;
}