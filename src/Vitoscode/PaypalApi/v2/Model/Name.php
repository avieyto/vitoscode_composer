<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:02 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Name
{
    /**
     * @var string $prefix
     */
    public $prefix;

    /**
     * @var string $given_name
     */
    public $given_name;

    /**
     * @var string $surname
     */
    public $surname;

    /**
     * @var string $middle_name
     */
    public $middle_name;

    /**
     * @var string $suffix
     */
    public $suffix;

    /**
     * @var string $alternate_full_name
     */
    public $alternate_full_name;

    /**
     * @var string $full_name
     */
    public $full_name;
}