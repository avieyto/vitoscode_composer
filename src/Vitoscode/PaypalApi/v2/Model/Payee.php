<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:19 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Payee
{
    /**
     * @var string $email_address
     */
    public $email_address;

    /**
     * @var string $merchant_id
     */
    public $merchant_id;
}