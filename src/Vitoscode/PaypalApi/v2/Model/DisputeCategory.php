<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 8:57 AM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class DisputeCategory
{
    /**
     * @var string $dispute_category
     *
     * The condition that is covered for the transaction
     *
     * ITEM_NOT_RECEIVED. The payer paid for an item that he or she did not receive.
     *
     * UNAUTHORIZED_TRANSACTION. The payer did not authorize the payment.
     */
    public $dispute_category;
}