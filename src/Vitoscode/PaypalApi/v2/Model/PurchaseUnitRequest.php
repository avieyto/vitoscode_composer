<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 3:58 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PurchaseUnitRequest
{
    /**
     * @var string $reference_id
     *
     * The API caller-provided external ID for the purchase unit. Required for multiple purchase units.
     */
    public $reference_id;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Amount $amount
     */
    public $amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Payee $payee
     *
     * The merchant who receives payment for this transaction.
     */
    public $payee;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PaymentInstruction $payment_instruction
     *
     * Any additional payment instructions for PayPal for Partner customers. Enables features for partners and marketplaces,
     * such as delayed disbursement and collection of a platform fee. Applies during order creation for captured payments or
     * during capture of authorized payments.
     */
    public $payment_instruction;

    /**
     * @var string $description
     *
     * The purchase description.
     */
    public $description;

    /**
     * @var string $custom_id
     *
     * The API caller-provided external ID.
     * Used to reconcile client transactions with PayPal transactions.
     * Appears in transaction and settlement reports but is not visible to the payer.
     */
    public $custom_id;

    /**
     * @var string $invoice_id
     *
     * The API caller-provided external invoice number for this order.
     * Appears in both the payer's transaction history and the emails that the payer receives.
     */
    public $invoice_id;

    /**
     * @var string $soft_descriptor
     *
     * The payment descriptor on account transactions on the customer's credit card statement. The maximum length of
     * the soft descriptor is 22 characters. Of this, the PayPal prefix uses eight characters (PAYPAL *). So, the
     * maximum length of the soft descriptor is:
     *
     * If the total length of the soft_descriptor exceeds 22 characters, the overflow is truncated.
     * For example, if:
     * The PayPal prefix toggle is PAYPAL *.
     * The merchant descriptor in the profile is VENMO.
     * The soft descriptor is JanesFlowerGifts LLC.
     * Then, the descriptor on the credit card is PAYPAL *VENMO JanesFlo.
     */
    public $soft_descriptor;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Item[] $items
     *
     * An array of items that the customer purchases from the merchant.
     */
    public $items;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Shipping $shipping
     *
     * The name and address of the person to whom to ship the items.
     */
    public $shipping;
}