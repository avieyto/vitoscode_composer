<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 3:09 PM
 */

namespace Vitoscode\PaypalApi\v2\Model\Request;


class AuthorizePaymentRequest
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PaymentSource $payment_source
     *
     * The source of payment for the order, which can be a token or a card.
     * Use this object only if you have not redirected the user after order
     * creation to approve the payment. In such cases, the user-selected payment
     * method in the PayPal flow is implicitly used.
     */
    public $payment_source;
}