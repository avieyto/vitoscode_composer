<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 9:01 AM
 */

namespace Vitoscode\PaypalApi\v2\Model\Request;


class CaptureAuthorizedPaymentRequest
{
    /**
     * @var string $invoice_id
     */
    public $invoice_id;

    /**
     * @var string $note_to_payer
     */
    public $note_to_payer;

    /**
     * @var string $soft_descriptor
     */
    public $soft_descriptor;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $amount
     */
    public $amount;

    /**
     * @var boolean $final_capture
     */
    public $final_capture;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PaymentInstruction $payment_instruction
     */
    public $payment_instruction;
}