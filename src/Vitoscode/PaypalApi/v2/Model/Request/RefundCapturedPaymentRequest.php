<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 10:24 AM
 */

namespace Vitoscode\PaypalApi\v2\Model\Request;


class RefundCapturedPaymentRequest
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $amount
     */
    public $amount;

    /**
     * @var string $invoice_id
     */
    public $invoice_id;

    /**
     * @var string $note_to_payer
     */
    public $note_to_payer;
}