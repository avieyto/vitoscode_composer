<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 3:19 PM
 */

namespace Vitoscode\PaypalApi\v2\Model\Request;

class CreateOrderRequest
{
    /**
     * @var string $intent
     *
     * CAPTURE: The merchant intends to capture payment immediately after the customer makes a payment.
     *
     * AUTHORIZE: The merchant intends to authorize a payment and place fund on hold after the customer makes a payment. Authorized
     * payments are guaranteed for up to three days but are available to capture for up 29 days. After the three-day honor period,
     * the original authorized payment expires and you must re-authorize the payment. You must make a separate request to capture payments on demand
     */
    public $intent;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Payer $payer
     */
    public $payer;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PurchaseUnitRequest[] $purchase_units
     */
    public $purchase_units;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\ApplicationContext $application_context
     */
    public $application_context;
}