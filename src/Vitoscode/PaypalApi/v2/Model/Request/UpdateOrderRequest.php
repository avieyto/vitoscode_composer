<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 2:31 PM
 */

namespace Vitoscode\PaypalApi\v2\Model\Request;


class UpdateOrderRequest
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PatchRequest[] $patch_request
     */
    public $patch_request;
}