<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:19 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Amount
{
    /**
     * @var string $currency_code
     */
    public $currency_code;

    /**
     * @var float $value
     */
    public $value;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Breakdown $breakdown
     */
    public $breakdown;
}