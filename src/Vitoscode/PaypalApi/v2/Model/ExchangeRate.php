<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 10:04 AM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class ExchangeRate
{
    /**
     * @var string $source_currency
     */
    public $source_currency;

    /**
     * @var string $target_currency
     */
    public $target_currency;

    /**
     * @var string $value
     */
    public $value;
}