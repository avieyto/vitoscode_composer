<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:20 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PaymentInstruction
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PlatformFee[] $platform_fees
     */
    public $platform_fees;

    /**
     * @var string $disbursement_mode
     *
     * INSTANT The funds are released to the merchant immediately.
     * DELAYED The funds are held for a finite number of days.
     */
    public $disbursement_mode;
}