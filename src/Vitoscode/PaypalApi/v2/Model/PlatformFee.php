<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 1:13 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PlatformFee
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $amount
     */
    public $amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Payee $payee
     */
    public $payee;
}