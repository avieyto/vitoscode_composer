<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 3:22 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Card
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $number
     */
    public $number;

    /**
     * @var string $expiry
     *
     * YYYY-MM
     */
    public $expiry;

    /**
     * @var string $security_code
     */
    public $security_code;

    /**
     * @var string $last_digits
     */
    public $last_digits;

    /**
     * @var string $card_type
     *
     * VISA
     * MASTERCARD
     * DISCOVER
     * AMEX
     * SOLO
     * JCB
     * STAR
     * DELTA
     * SWITCH
     * MAESTRO
     * CB_NATIONALE
     * CONFIGOGA
     * CONFIDIS
     * ELECTRON
     * CETELEM
     * CHINA_UNION_PAY
     */
    public $card_type;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Address $billing_address
     */
    public $billing_address;
}