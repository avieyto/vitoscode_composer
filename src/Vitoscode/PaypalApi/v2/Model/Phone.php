<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:04 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Phone
{
    /**
     * @var string $country_code
     */
    public $country_code;

    /**
     * @var string $national_number
     */
    public $national_number;

    /**
     * @var string $extension_number
     */
    public $extension_number;
}