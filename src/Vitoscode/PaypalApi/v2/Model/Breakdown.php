<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:29 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Breakdown
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $item_total
     */
    public $item_total;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $shipping
     */
    public $shipping;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $handling
     */
    public $handling;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $tax_total
     */
    public $tax_total;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $insurance
     */
    public $insurance;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $shipping_discount
     */
    public $shipping_discount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $discount
     */
    public $discount;
}