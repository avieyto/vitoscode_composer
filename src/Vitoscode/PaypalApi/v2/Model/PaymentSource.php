<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 3:11 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PaymentSource
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Card $card
     */
    public $card;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PaymentSourceToken $token
     */
    public $token;
}