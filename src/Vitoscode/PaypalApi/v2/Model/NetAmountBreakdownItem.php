<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 10:39 AM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class NetAmountBreakdownItem
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $payable_amount
     *
     * The net amount debited from the merchant's PayPal account.
     */
    public $payable_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $converted_amount
     *
     * The converted payable amount.
     */
    public $converted_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\ExchangeRate $exchange_rate
     *
     * The exchange rate that determines the amount that was debited from the merchant's PayPal account.
     */
    public $exchange_rate;
}