<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:07 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class TaxInfo
{
    /**
     * @var string $tax_id
     */
    public $tax_id;

    /**
     * @var string $tax_id_type
     *
     * BR_CPF => The individual tax ID type.
     * BR_CNPJ => The business tax ID type.
     */
    public $tax_id_type;
}