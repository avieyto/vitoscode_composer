<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 2:03 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class HateoasLink
{
    /**
     * @var string $href
     */
    public $href;

    /**
     * @var string $rel
     *
     */
    public $rel;

    /**
     * @var string $method
     */
    public $method;
}