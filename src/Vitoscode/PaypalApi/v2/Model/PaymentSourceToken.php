<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 3:25 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PaymentSourceToken
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var string $type
     *
     * The tokenization method that was used to generate the ID.
     *
     * NONCE: A secure, one-time-use reference to a payment source, such as payment card, PayPal account, and so on.
     *
     * PAYMENT_METHOD_TOKEN: The payment method token, which is a reference to a transactional payment source.
     * Typically stored on the merchant's server.
     *
     * BILLING_AGREEMENT: The PayPal billing agreement ID. References an approved recurring payment for goods or
     * services.
     *
     * FUNDING_OPTION_ID: The PayPal funding option ID. Represents a payment plan identifier computed based on buyer
     * wallet, seller account and transaction currency.
     */
    public $type;
}