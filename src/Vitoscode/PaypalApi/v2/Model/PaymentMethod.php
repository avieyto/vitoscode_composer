<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 1:40 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class PaymentMethod
{
    CONST PAYER_SELECTED_PAYPAL = 'PAYPAL';
    CONST PAYER_SELECTED_PAYPAL_CREDIT = 'PAYPAL_CREDIT';
    CONST PAYEE_PREFERRED_UNRESTRICTED = 'UNRESTRICTED';
    CONST PAYEE_PREFERRED_IMMEDIATE_PAYMENT_REQUIRED = 'IMMEDIATE_PAYMENT_REQUIRED';
    /**
     * @var string $payer_selected
     *
     * The customer-selected payment method on the merchant site.
     *
     * PAYPAL_CREDIT: When the payer chooses PayPal Credit as the payment method
     *
     * PAYPAL: When the payer chooses PayPal as the payment method.
     */
    public $payer_selected;

    /**
     * @var string $payee_preferred
     *
     * The merchant-preferred payment sources.
     *
     * UNRESTRICTED: Accepts any type of payment from the customer.
     *
     * IMMEDIATELY_PAYMENT_REQUIRED: Accepts only immediate payment from the customer.
     * For example: Credit Cards, Paypal Balance, or Instant ACH.
     * Ensures that at the time of capture, the payment does not have a 'pending' status.
     */
    public $payee_preferred;
}