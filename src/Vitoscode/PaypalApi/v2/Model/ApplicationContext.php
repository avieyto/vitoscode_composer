<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 3:58 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class ApplicationContext
{
    CONST LANDING_PAGE_LOGIN = 'LOGIN';
    CONST LANDING_PAGE_BILLING = 'BILLING';
    CONST SHIPPING_ADDRESS_PREFERENCE_GET_FROM_FILE = 'GET_FROM_FILE';
    CONST SHIPPING_ADDRESS_PREFERENCE_NO_SHIPPING = 'NO_SHIPPING';
    CONST SHIPPING_ADDRESS_PREFERENCE_SET_PROVIDED_ADDRESS = 'SET_PROVIDED_ADDRESS';
    CONST USER_ACTION_CONTINUE = 'CONTINUE';
    CONST USER_ACTION_PAY_NOW = 'PAY_NOW';

    /**
     * @var string $brand_name
     */
    public $brand_name;

    /**
     * @var string $locale
     */
    public $locale;

    /**
     * @var string $landing_page
     *
     * LOGIN: When the customer clicks Paypal Checkout, the customer is redirected to a page to log in to
     * Paypal and approve the payment.
     *
     * BILLING: When the customer clicks Paypal Checkout, the customer is redirected to a page
     * to enter credit or debit card and other relevant billing information required
     * to complete the purchase.
     */
    public $landing_page;

    /**
     * @var string $shipping_preference
     *
     * GET_FROM_FILE: Use the customer-provided shipping address on the Paypal Site.
     *
     * NO_SHIPPING: Redact the shipping address from the Paypal site. Recommended for
     * digital goods.
     *
     * SET_PROVIDED_ADDRESS: Use the merchant-provided address. The customer cannot
     * change this address on the Paypal site.
     */
    public $shipping_preference;

    /**
     * @var string $user_action
     *
     * CONTINUE: After you redirect the customer to the paypal payment page, a continue
     * button appears. Use this option when the final amount is not known  when the checkout
     * flow is initiated and you want to redirect the customer to the merchant page without
     * processing the payment.
     *
     * PAY_NOW: After you redirect the customer to the paypal payment page, a Pay Now button
     * appears. Use this option when the final amount is known when the checkout is initiated and you
     * want to process the payment immediately when the customer clicks Pay Now.
     */
    public $user_action;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PaymentMethod $payment_method
     */
    public $payment_method;

    /**
     * @var string $return_url
     */
    public $return_url;

    /**
     * @var string $cancel_url
     */
    public $cancel_url;
}