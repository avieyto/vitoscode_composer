<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 3:58 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Payer
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Name $name
     */
    public $name;

    /**
     * @var string $email_address
     */
    public $email_address;

    /**
     * @var string $payer_id
     */
    public $payer_id;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Phone $phone
     */
    public $phone;

    /**
     * @var string $birth_date
     *
     * Format (YYYY-MM-DD)
     */
    public $birth_date;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\TaxInfo $tax_info
     */
    public $tax_info;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Address $address
     */
    public $address;
}