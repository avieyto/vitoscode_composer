<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 10:00 AM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class SellerReceivableBreakdown
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $gross_amount
     *
     * The amount for this captured payment.
     */
    public $gross_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $paypal_fee
     *
     * The applicable fee for this captured payment.
     */
    public $paypal_fee;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $net_amount
     *
     * The net amount that the payee receives for this captured payment in their PayPal account.
     * The net amount is computed as gross_amount minus the paypal_fee minus the platform_fees.
     */
    public $net_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $receivable_amount
     *
     * The net amount that is credited to the payee's PayPal account. Returned only when the currency of the captured
     * payment is different from the currency of the PayPal account where the payee wants to credit the funds.
     * The amount is computed as net_amount times exchange_rate.
     */
    public $receivable_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\ExchangeRate $exchange_rate
     *
     * The exchange rate that determines the amount that is credited to the payee's PayPal account. Returned when the
     * currency of the captured payment is different from the currency of the PayPal account where the payee wants to
     * credit the funds.
     */
    public $exchange_rate;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PlatformFee[] $platform_fees
     *
     * An array of platform or partner fees, commissions, or brokerage fees that associated with the captured payment.
     */
    public $platform_fees;
}