<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 4:24 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Item
{
    /**
     * @var string $name
     */
    public $name;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $unit_amount
     */
    public $unit_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $tax
     */
    public $tax;

    /**
     * @var integer $quantity
     */
    public $quantity;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $sku
     */
    public $sku;

    /**
     * @var string $category
     *
     * DIGITAL_GOODS
     * PHYSICAL_GOODS
     */
    public $category;
}