<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 8:55 AM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class SellerProtection
{
    /**
     * @var string $status
     *
     * Indicates whether the transaction is eligible for seller protection.
     *
     * ELIGIBLE. Your PayPal balance remains intact if the customer claims that he or she did not receive an item or
     * the account holder claims that he or she did not authorize the payment.
     *
     * PARTIALLY_ELIGIBLE. Your PayPal balance remains intact if the customer claims that he or she did not receive
     * an item.
     *
     * NOT_ELIGIBLE. This transaction is not eligible for seller protection.
     */
    public $status;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\DisputeCategory[] $dispute_categories
     *
     * An array of conditions that are covered for the transaction.
     */
    public $dispute_categories;
}