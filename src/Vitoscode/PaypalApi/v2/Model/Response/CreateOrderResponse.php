<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 1:52 PM
 */

namespace Vitoscode\PaypalApi\v2\Model\Response;


class CreateOrderResponse
{
    /**
     * @var string $create_time
     */
    public $create_time;

    /**
     * @var string $update_time
     */
    public $update_time;

    /**
     * @var string $id
     */
    public $id;

    /**
     * @var string $intent
     *
     * CAPTURE:
     *
     * AUTHORIZE:
     */
    public $intent;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Payer $payer
     */
    public $payer;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PurchaseUnitRequest[] $purchase_units
     */
    public $purchase_units;

    /**
     * @var string $status
     *
     * CREATED:
     * SAVED:
     * APPROVED:
     * VOIDED:
     * COMPLETED:
     */
    public $status;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\HateoasLink[] $links
     */
    public $links;
}