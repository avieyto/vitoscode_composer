<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 8:46 AM
 */

namespace Vitoscode\PaypalApi\v2\Model\Response;


class ShowDetailsAuthorizedPaymentResponse
{
    /**
     * @var string $status
     *
     * CREATED. The authorized payment is created. No captured payments have been made for this authorized payment.
     *
     * CAPTURED. The authorized payment has one or more captures against it. The sum of these captured payments
     * is greater than the amount of the original authorized payment.
     *
     * DENIED. PayPal cannot authorize funds for this authorized payment.
     *
     * EXPIRED. The authorized payment has expired.
     *
     * PARTIALLY_CAPTURED. A captured payment was made for the authorized payment for an amount that is less than the
     * amount of the original authorized payment.
     *
     * VOIDED. The authorized payment was voided. No more captured payments can be made against this authorized payment.
     *
     * PENDING. The created authorization is in pending state. For more information, see status.details
     */
    public $status;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\StatusDetails $status_details
     */
    public $status_details;

    /**
     * @var string $id
     */
    public $id;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $amount
     */
    public $amount;

    /**
     * @var string $invoice_id
     */
    public $invoice_id;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\SellerProtection $seller_protection
     */
    public $seller_protection;

    /**
     * @var string $expiration_time
     */
    public $expiration_time;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\HateoasLink[] $links
     */
    public $links;

    /**
     * @var string $create_time
     */
    public $create_time;

    /**
     * @var string $update_time
     */
    public $update_time;
}