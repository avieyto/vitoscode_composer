<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 10:26 AM
 */

namespace Vitoscode\PaypalApi\v2\Model\Response;


class RefundCapturedPaymentResponse
{
    /**
     * @var string $status
     *
     * CANCELLED. The refund was cancelled.
     *
     * PENDING. The refund is pending. For more information, see status_details.reason.
     *
     * COMPLETED. The funds for this transaction were debited to the customer's account.
     */
    public $status;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\StatusDetails $status_details
     */
    public $status_details;

    /**
     * @var string $id
     */
    public $id;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $amount
     */
    public $amount;

    /**
     * @var string $invoice_id
     */
    public $invoice_id;

    /**
     * @var string $note_to_payer
     */
    public $note_to_payer;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\SellerPayableBreakdown $seller_payable_breakdown
     */
    public $seller_payable_breakdown;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\HateoasLink[] $links
     */
    public $links;

    /**
     * @var string $create_time
     */
    public $create_time;

    /**
     * @var string $update_time
     */
    public $update_time;
}