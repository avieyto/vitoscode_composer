<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 9:01 AM
 */

namespace Vitoscode\PaypalApi\v2\Model\Response;


class CaptureAuthorizedPaymentResponse extends ShowDetailsAuthorizedPaymentResponse
{
    /**
     * @var boolean $final_capture
     */
    public $final_capture;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\SellerReceivableBreakdown $seller_receivable_breakdown
     */
    public $seller_receivable_breakdown;

    /**
     * @var string $disbursement_mode
     *
     * The funds that are held on behalf of the merchant.
     *
     * INSTANT. The funds are released to the merchant immediately.
     * DELAYED. The funds are held for a finite number of days. The actual duration depends on the region and type
     * of integration. You can release the funds through a referenced payout. Otherwise, the funds disbursed
     * automatically after the specified duration.
     */
    public $disbursement_mode;
}