<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/3/2019
 * Time: 3:20 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Token
{
    /**
     * @var string $access_token
     */
    public $access_token;

    /**
     * @var string $token_type
     */
    public $token_type;

    /**
     * @var string $app_id
     */
    public $app_id;

    /**
     * @var integer $expires_in
     */
    public $expires_in;

    /**
     * @var string $nonce
     */
    public $nonce;

    /**
     * @var string $scope
     */
    public $scope;

    /**
     * @var int
     */
    public $created;

    /**
     * PaypalToken constructor.
     */
    public function __construct()
    {
        $this->created = time();
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $expiration = $this->created + $this->expires_in;
        return $expiration < time();
    }
}