<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/6/2019
 * Time: 10:35 AM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class SellerPayableBreakdown
{
    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $gross_amount
     *
     * The amount that the payee refunded to the payer.
     */
    public $gross_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $paypal_fee
     *
     * The PayPal fee that was refunded to the payer. This fee might not match the PayPal fee that the payee paid
     * when the payment was captured.
     */
    public $paypal_fee;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $net_amount
     *
     * The net amount that the payee's account is debited, if the payee holds funds in the currency for this refund.
     * The net amount is calculated as gross_amount minus paypal_fee minus platform_fees.
     */
    public $net_amount;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\PlatformFee[] $platform_fess
     *
     * An array of platform or partner fees, commissions, or brokerage fees for the refund.
     */
    public $platform_fess;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\NetAmountBreakdownItem[] $net_amount_breakdown
     *
     * An array of breakdown values for the net amount. Returned when the currency of the refund is different from
     * the currency of the PayPal account where the payee holds their funds.
     */
    public $net_amount_breakdown;

    /**
     * @var \Vitoscode\PaypalApi\v2\Model\Money $total_refunded_amount
     *
     * The total amount refunded from the original capture to date. For example, if a payer makes a $100 purchase and
     * was refunded $20 a week ago and was refunded $30 in this refund, the gross_amount is $30 for this refund and
     * the total_refunded_amount is $50.
     */
    public $total_refunded_amount;
}