<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 1:24 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class ShippingNameDetail
{
    /**
     * @var string $full_name
     */
    public $full_name;
}