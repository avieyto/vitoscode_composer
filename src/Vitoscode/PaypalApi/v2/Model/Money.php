<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 5/5/2019
 * Time: 1:07 PM
 */

namespace Vitoscode\PaypalApi\v2\Model;


class Money
{
    /**
     * @var string $currency_code
     */
    public $currency_code;

    /**
     * @var float $value
     */
    public $value;
}