<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/21/2019
 * Time: 2:05 PM
 */

namespace Vitoscode\PaypalApi\v2;


use Vitoscode\Commons\Environment\ApiEnvironment;
use Vitoscode\Commons\Environment\IApiEnvironment;
use Vitoscode\PaypalApi\IPaypalApiEnvironment;

class PaypalApiEnvironment extends ApiEnvironment implements IPaypalApiEnvironment
{
    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->getVarEnvironmentActive('urlBase', '');
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->getVarEnvironmentActive('apiKey', '');
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->getVarEnvironmentActive('apiSecret', '');
    }

    /**
     * @return string
     */
    public function getWebExperienceId()
    {
        return $this->getVarEnvironmentActive('webExperienceId', '');
    }
}