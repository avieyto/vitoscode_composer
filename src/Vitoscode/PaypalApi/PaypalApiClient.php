<?php

namespace Vitoscode\PaypalApi;

use GuzzleHttp\Exception\GuzzleException;
use Vitoscode\Commons\Environment\IApiEnvironment;
use Vitoscode\Decoder\JsonDecoder;
use Vitoscode\PaypalApi\Exception\PaypalException;
use Vitoscode\PaypalApi\Model\PaypalToken;
use Vitoscode\PaypalApi\Request\CapturePaymentRequest;
use Vitoscode\PaypalApi\Request\CreatePaymentRequest;
use Vitoscode\PaypalApi\Request\CreateWebExperienceProfileRequest;
use Vitoscode\PaypalApi\Request\ExecutePaymentRequest;
use Vitoscode\PaypalApi\Response\CapturePaymentResponse;
use Vitoscode\PaypalApi\Response\CreatePaymentResponse;
use Vitoscode\PaypalApi\Response\CreateWebExperienceProfileResponse;
use Vitoscode\PaypalApi\Response\ExecutePaymentResponse;
use Vitoscode\PaypalApi\Response\PaypalError;
use Vitoscode\PaypalApi\Response\VoidPaymentResponse;
use Vitoscode\Commons\BaseRestApiClient;
use Vitoscode\PaypalApi\TokenStorage\ITokenStorageHandler;

/**
 * Description of PaypalApiClient
 *
 * @author gateway
 */
class PaypalApiClient extends BaseRestApiClient implements IPaypalApiClient
{
    /**
     * @var string $apiKey
     */
    protected $apiKey;

    /**
     * @var string $apiPassword
     */
    protected $apiPassword;

    /**
     * @var string $id_web_experience
     */
    protected $id_web_experience = '';

    //protected $customer_urls;

    /**
     * @var IPaypalUrlManager $paypalUrlManager
     */
    protected $paypalUrlManager;

    /**
     * @var ITokenStorageHandler $tokenStorageHandler
     */
    protected $tokenStorageHandler;

    /**
     * PaypalApiClient constructor.
     * @param $apiName
     * @param IApiEnvironment $apiEnvironments
     * @param IPaypalUrlManager|null $paypalUrlManager
     * @param ITokenStorageHandler $tokenStorageHandler
     * @throws \Exception
     */
    public function __construct($apiName, IApiEnvironment $apiEnvironments, IPaypalUrlManager $paypalUrlManager = null, ITokenStorageHandler $tokenStorageHandler = null)
    {
        if (!($apiEnvironments instanceof IPaypalApiEnvironment)) {
            throw new \Exception("Invalid Paypal Api Environment, apiEnvironments must be a class than implement IPaypalApiEnvironment");
        }

        if ($paypalUrlManager)
            $this->paypalUrlManager = $paypalUrlManager;
        else {
            $url = $apiEnvironments->getBaseUrl();
            if (!$url)
                $url = $apiEnvironments->isDev()?'https://api.sandbox.paypal.com/v1':'https://api.paypal.com/v1';
            $this->paypalUrlManager = new PaypalUrlManager($url);
        }

        $this->setTokenStorageHandler($tokenStorageHandler);

        parent::__construct($apiName, $apiEnvironments);
        $this->id_web_experience = $apiEnvironments->getWebExperienceId();
        $this->apiKey = $apiEnvironments->getApiKey();
        $this->apiPassword = $apiEnvironments->getApiSecret();
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        $actualToken = $this->tokenStorageHandler->retrieveToken();
        if ($actualToken && !$actualToken->isExpired())
            return $actualToken->access_token;
        else {
            $actualToken = $this->createAccessToken($this->apiKey, $this->apiPassword);
            if ($actualToken && !$actualToken->isExpired()) {
                $this->tokenStorageHandler->storeToken($actualToken);
                return $actualToken->access_token;
            }
            else
                throw new PaypalException("An error occurred creating the paypal access access_token", -1, null, null);
        }
    }

    /**
     * @inheritdoc
     */
    public function createAccessToken($apiKey, $apiSecret)
    {
        $url = $this->paypalUrlManager->getGetTokenUrl();
        $data = 'grant_type=client_credentials';
        $options = [
            'headers' => [
                'User-Agent' => 'PHP Vitoscode Paypal Api Client/1.0',
                'Accept' => 'application/json',
                'Accept-Languages' => 'en_US',
                'Content-Type' => 'application/json'
            ],
            'body' => $data,
            'verify' => false,
            'auth' => [$this->apiKey, $this->apiPassword]
        ];

        $className = 'Vitoscode\PaypalApi\Model\PaypalToken';
        /**
         * @var $response PaypalToken
         */
        $response = $this->sendRequest('GET_TOKEN', 'POST', $url, $options, $className, null);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function createPaymentWebExperience(CreateWebExperienceProfileRequest $createWebExperienceProfile)
    {
        $token = $this->getAccessToken();
        $url = $this->paypalUrlManager->getCreateWebExperienceUrl();
        $data = $createWebExperienceProfile->encodeToJson();
        $options = [
            'headers' => [
                'User-Agent' => 'PHP Vitoscode Paypal Api Client/1.0',
                'Accept' => 'application/json',
                'Accept-Languages' => 'en_US',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'body' => $data,
            'verify' => false
        ];
        $className = 'Vitoscode\PaypalApi\Response\CreateWebExperienceProfileResponse';
        /**
         * @var $response CreateWebExperienceProfileResponse
         */
        $response = $this->sendRequest('CREATE_WEB_EXPERIENCE', 'POST', $url, $options, $className, null);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function createPayment(CreatePaymentRequest $createPaymentRequest)
    {
        $token = $this->getAccessToken();
        $url = $this->paypalUrlManager->getCreatePaymentUrl();
        $data = $createPaymentRequest->encodeToJson();
        $options = [
            'headers' => [
                'User-Agent' => 'PHP Vitoscode Paypal Api Client/1.0',
                'Accept' => 'application/json',
                'Accept-Languages' => 'en_US',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'body' => $data,
            'verify' => false
        ];
        $className = 'Vitoscode\PaypalApi\Response\CreatePaymentResponse';
        /**
         * @var $response CreatePaymentResponse
         */
        $response = $this->sendRequest('CREATE_PAYMENT', 'POST', $url, $options, $className, null);
        if ($response instanceof CreatePaymentResponse)
            $response->tokenUsed = $token;
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function executePayment(ExecutePaymentRequest $payment, $paymentId, $token)
    {
        $url = $this->paypalUrlManager->getExecutePaymentUrl($paymentId);
        $data = $payment->encodeToJson();
        $options = [
            'headers' => [
                'User-Agent' => 'PHP Vitoscode Paypal Api Client/1.0',
                'Accept' => 'application/json',
                'Accept-Languages' => 'en_US',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'body' => $data,
            'verify' => false
        ];
        $className = 'Vitoscode\PaypalApi\Response\ExecutePaymentResponse';
        /**
         * @var $response ExecutePaymentResponse
         */
        $response = $this->sendRequest('EXECUTE_PAYMENT', 'POST', $url, $options, $className, null);
        return $response;
    }

    function capturePayment(CapturePaymentRequest $capturePayment, $authorizationId, $token)
    {
        $url = $this->paypalUrlManager->getCapturePaymentUrl($authorizationId);
        $data = $capturePayment->encodeToJson();
        $options = [
            'headers' => [
                'User-Agent' => 'PHP Vitoscode Paypal Api Client/1.0',
                'Accept' => 'application/json',
                'Accept-Languages' => 'en_US',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'body' => $data,
            'verify' => false
        ];
        $className = 'Vitoscode\PaypalApi\Response\CapturePaymentResponse';
        /**
         * @var $response CapturePaymentResponse
         */
        $response = $this->sendRequest('CAPTURE_PAYMENT', 'POST', $url, $options, $className, null);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function voidPayment($authorizationId, $token)
    {
        $url = $this->paypalUrlManager->getVoidPaymentUrl($authorizationId);
        $options = [
            'headers' => [
                'User-Agent' => 'PHP Vitoscode Paypal Api Client/1.0',
                'Accept' => 'application/json',
                'Accept-Languages' => 'en_US',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'body' => '',
            'verify' => false
        ];
        $className = 'Vitoscode\PaypalApi\Response\VoidPaymentResponse';
        /**
         * @var $response VoidPaymentResponse
         */
        $response = $this->sendRequest('VOID_PAYMENT', 'POST', $url, $options, $className, null);
        return $response;
    }

    public function refundPayment()
    {
        throw new PaypalException('Not implemented');
    }

    /**
     * @inheritdoc
     */
    public function setTokenStorageHandler(ITokenStorageHandler $handler)
    {
        $this->tokenStorageHandler = $handler;
    }

    /**
     * @inheritdoc
     */
    public function getTokenStorageHandler()
    {
        return $this->tokenStorageHandler;
    }

    /**
     * @param string $methodName
     * @param string $requestType
     * @param string $url
     * @param array $options
     * @param string $responseClass
     * @param object $context
     * @return object|PaypalError|null
     * @throws PaypalException
     */
    protected function sendRequest($methodName, $requestType, $url, $options, $responseClass, $context)
    {
        try {
            $guzzleResponse = $this->request($methodName, $requestType, $url, $options, $context);
            if ($guzzleResponse) {
                $statusCode = $guzzleResponse->getStatusCode();
                $reasonPhrase = $guzzleResponse->getReasonPhrase();
                $responseString = $guzzleResponse->getBody()->getContents();
                if ($statusCode != 200)
                    throw new PaypalException('An error occurred requesting paypal ' . $methodName . ', Reason: ' . $reasonPhrase, $statusCode, null, $responseString);
                $jsonObject = json_decode($responseString);
                if (!$jsonObject)
                    throw new PaypalException('Invalid response returned by paypal on ' . $methodName, 0, null, $responseString);

                if (isset($jsonObject->id)) {
                    $response = JsonDecoder::JsonObject2Object($responseClass, $jsonObject);
                    return $response;
                } else {
                    $responseClass = 'Vitoscode\PaypalApi\Response\PaypalError';
                    /**
                     * @var $response PaypalError
                     */
                    $response = JsonDecoder::JsonObject2Object($responseClass, $jsonObject);
                    return $response;
                }
            } else
                throw new PaypalException('An error occurred requesting paypal ' . $methodName, 0, null, $guzzleResponse);
        }
        catch(GuzzleException $exception) {
            throw new PaypalException($exception->getMessage(), $exception->getCode(), $exception, null);
        }
    }
}
