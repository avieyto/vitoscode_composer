<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/26/2019
 * Time: 10:38 AM
 */

namespace Vitoscode\PaypalApi;


use Vitoscode\PaypalApi\Exception\IPaypalException;
use Vitoscode\PaypalApi\Exception\PaypalException;

class PaypalUrlManager implements IPaypalUrlManager
{
    /**
     * @var string $baseUrl
     */
    protected $baseUrl;

    /**
     * @var string $environment
     */
    protected $environment;

    /**
     * @var array $url_access_suffix
     */
    protected $url_access_suffix = array(
        "get_token" => "/oauth2/access_token",
        "create_payment" => "/payments/payment",
        "execute_payment" => "/payments/payment/{paymentId}/execute",
        "get_payment" => "/payments/payment/{paymentId}",
        "capture_payment" => "/payments/authorization/{authorizationId}/capture",
        "void_payment" => "/payments/authorization/{authorizationId}/void",
        "payment_experience" => "/payment-experience/web-profiles",
    );

    /**
     * PaypalUrlManager constructor.
     * @param $baseUrl
     */
    public function __construct($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getCreatePaymentUrl()
    {
        return $this->getAccessUrl('create_payment');
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getExecutePaymentUrl($paymentId)
    {
        $url = $this->getAccessUrl('execute_payment');
        $url = str_replace('{paymentId}', urlencode($paymentId), $url);
        return $url;
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getCapturePaymentUrl($authorizationId)
    {
        $url = $this->getAccessUrl('capture_payment');
        $url = str_replace('{authorizationId}', urlencode($authorizationId), $url);
        return $url;
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getVoidPaymentUrl($authorizationId)
    {
        $url = $this->getAccessUrl('void_payment');
        $url = str_replace('{authorizationId}', urlencode($authorizationId), $url);
        return $url;
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getGetPaymentUrl($paymentId)
    {
        $url = $this->getAccessUrl('get_payment');
        $url = str_replace('{paymentId}', urlencode($paymentId), $url);
        return $url;
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getGetTokenUrl()
    {
        return $this->getAccessUrl('get_token');
    }

    /**
     * @inheritdoc
     * @throws IPaypalException
     */
    public function getCreateWebExperienceUrl()
    {
        return $this->getAccessUrl('payment_experience');
    }

    /**
     * @param $access_key
     * @return string
     * @throws IPaypalException
     */
    protected function getAccessUrl($access_key)
    {
        if (isset($this->url_access_suffix[$access_key]))
        {
            return $this->baseUrl.$this->url_access_suffix[$access_key];
        }
        throw new PaypalException("The access key $access_key don't exist");
    }
}