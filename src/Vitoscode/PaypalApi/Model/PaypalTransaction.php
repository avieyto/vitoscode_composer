<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 9:18 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalTransaction
{
    /**
     * @var string $reference_id
     */
    public $reference_id;

    /**
     * @var PaypalAmount $amount
     */
    public $amount;

    /**
     * @var PaypalPayee $payee
     */
    public $payee;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $note_to_payee
     */
    public $note_to_payee;

    /**
     * @var string $custom
     */
    public $custom;

    /**
     * @var string $invoice_number
     */
    public $invoice_number;

    /**
     * @var string $purchase_order
     */
    public $purchase_order;

    /**
     * @var string $soft_descriptor
     */
    public $soft_descriptor;

    /**
     * @var PaypalPaymentOptions $payment_options
     */
    public $payment_options;

    /**
     * @var PaypalItemList $item_list
     */
    public $item_list;

    /**
     * @var string $notify_url
     */
    public $notify_url;

    /**
     * @var string $order_url
     */
    public $order_url;

    /**
     * @var array $related_resources
     */
    public $related_resources;
}