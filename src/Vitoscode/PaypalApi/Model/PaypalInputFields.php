<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 8:44 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalInputFields
{
    /**
     * @var int $no_shipping
     *
     * 0 - Displays the shipping address on the Paypal Pages
     * 1 - Redacts shipping address fields from Paypal Pages. For digital goods, this field is required and must be 1.
     * 2 - Gets the shipping address from the customer's account profile.
     */
    public $no_shipping;

    /**
     * @var int $address_override
     *
     * 0 - Displays the shipping address on file.
     * 1 - Displays the shipping address specified in this call. The customer cannot edit this shipping address
     */
    public $address_override;
}