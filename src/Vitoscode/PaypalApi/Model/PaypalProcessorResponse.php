<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/22/2018
 * Time: 10:54 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalProcessorResponse
{
    /**
     * @var string $response_code
     */
    public $response_code;

    /**
     * @var string $avs_code
     */
    public $avs_code;

    /**
     * @var string $cvv_code
     */
    public $cvv_code;

    /**
     * @var string $advice_code
     */
    public $advice_code;

    /**
     * @var string $eci_submitted
     */
    public $eci_submitted;

    /**
     * @var string $vpas
     */
    public $vpas;
}