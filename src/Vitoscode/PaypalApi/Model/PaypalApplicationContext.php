<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 6/15/2018
 * Time: 9:39 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalApplicationContext
{
    /**
     * @var string $brand_name
     */
    public $brand_name;

    /**
     * @var string $locale
     */
    public $locale;

    /**
     * @var string $landing_page
     */
    public $landing_page;

    /**
     * @var string $shipping_preferences
     */
    public $shipping_preferences;

    /**
     * @var string $user_action
     */
    public $user_action;
}