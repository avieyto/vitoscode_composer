<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 7/27/2017
 * Time: 11:42 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalLink
{
    /**
     * @var string $href
     */
    public $href;

    /**
     * @var string $method
     */
    public $method;

    /**
     * @var string $rel
     */
    public $rel;
}