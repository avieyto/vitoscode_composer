<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 8:44 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalPresentation
{
    /**
     * @var string $brand_name
     */
    public $brand_name;

    /**
     * @var string $logo_image
     */
    public $logo_image;

    /**
     * @var string $locale_code
     */
    public $locale_code;
}