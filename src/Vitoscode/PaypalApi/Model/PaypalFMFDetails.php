<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/22/2018
 * Time: 10:51 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalFMFDetails
{
    /**
     * @var string $filter_type
     */
    public $filter_type;

    /**
     * @var string $filter_id
     */
    public $filter_id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $description
     */
    public $description;
}