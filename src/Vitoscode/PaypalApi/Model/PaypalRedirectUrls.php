<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 9:19 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalRedirectUrls
{
    /**
     * @var string $return_url
     */
    public $return_url;

    /**
     * @var string $cancel_url
     */
    public $cancel_url;
}