<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 2:23 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalToken
{
    /**
     * @var string $access_token
     */
    public $access_token;

    /**
     * @var string $token_type
     */
    public $token_type;

    /**
     * @var int $created
     */
    public $created;

    /**
     * @var int $expires_in
     */
    public $expires_in;

    /**
     * PaypalToken constructor.
     */
    public function __construct()
    {
        $this->created = time();
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $expiration = $this->created + $this->expires_in;
        return $expiration < time();
    }
}