<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 4:01 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalItem
{
    /**
     * @var string $sku
     */
    public $sku;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var integer $quantity
     */
    public $quantity;

    /**
     * @var float $price
     */
    public $price;

    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var float $tax
     */
    public $tax;

    /**
     * @var string $url
     */
    public $url;
}