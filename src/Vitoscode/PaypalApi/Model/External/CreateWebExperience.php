<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/1/2019
 * Time: 2:15 PM
 */

namespace Vitoscode\PaypalApi\Model\External;


class CreateWebExperience
{
    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $landingPageType
     */
    public $landingPageType;

    /**
     * @var string $bankTxnPendingUrl
     */
    public $bankTxnPendingUrl;

    /**
     * @var string $allowNote
     */
    public $allowNote;

    /**
     * @var string $noShipping
     */
    public $noShipping;

    /**
     * @var string $addressOverride
     */
    public $addressOverride;

    /**
     * @var string $brandName
     */
    public $brandName;

    /**
     * @var string $logoImage
     */
    public $logoImage;

    /**
     * @var string $localeCode
     */
    public $localeCode;

    /**
     * @var string $token
     */
    public $token;
}