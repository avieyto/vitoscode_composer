<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:48 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalItemList
{
    /**
     * @var PaypalItem[] $items
     */
    public $items;

    /**
     * @var PaypalShippingAddress $shipping_address
     */
    public $shipping_address;

    /**
     * @var string $shipping_method
     */
    public $shipping_method;

    /**
     * @var string $shipping_phone_number
     */
    public $shipping_phone_number;

    /**
     * PaypalItemList constructor.
     */
    public function __construct()
    {
        $this->items = array();
    }
}