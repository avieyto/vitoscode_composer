<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/23/2018
 * Time: 3:00 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalTransactionFee
{
    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var float $value
     */
    public $value;
}