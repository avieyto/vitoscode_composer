<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:39 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalPayee
{
    /**
     * @var string $email
     */
    public $email;

    /**
     * @var string $merchant_id
     */
    public $merchant_id;

    /**
     * @var PaypalPayeeDisplayMetadata $payee_display_metadata
     */
    public $payee_display_metadata;
}