<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:34 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalAmountDetails
{
    /**
     * @var float $subtotal
     */
    public $subtotal;

    /**
     * @var float $shipping
     */
    public $shipping;

    /**
     * @var float $tax
     */
    public $tax;

    /**
     * @var float $handling_fee
     */
    public $handling_fee;

    /**
     * @var float $shipping_discount
     */
    public $shipping_discount;

    /**
     * @var float $insurance
     */
    public $insurance;

    /**
     * @var float $gift_wrap
     */
    public $gift_wrap;
}