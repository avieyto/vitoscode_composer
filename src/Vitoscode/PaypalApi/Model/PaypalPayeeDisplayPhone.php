<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:43 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalPayeeDisplayPhone
{
    /**
     * @var string $country_code
     */
    public $country_code;

    /**
     * @var string $number
     */
    public $number;
}