<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 8:43 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalFlowConfig
{
    /**
     * @var string $landing_page_type (Billing or Login)
     */
    public $landing_page_type;

    /**
     * @var string $bank_txn_pending_url
     */
    public $bank_txn_pending_url;

    /**
     * @var string $user_action (Continue => continue or pay now => commit)
     */
    public $user_action;

    /**
     * @var string $return_uri_http_method (POST, GET)
     */
    public $return_uri_http_method;
}