<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:41 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalPayeeDisplayMetadata
{
    /**
     * @var string $email
     */
    public $email;

    /**
     * @var PaypalPayeeDisplayPhone $display_phone
     */
    public $display_phone;

    /**
     * @var string $brand_name
     */
    public $brand_name;
}