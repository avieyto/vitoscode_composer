<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 4:00 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalShippingAddress
{
    /**
     * @var string $line1
     */
    public $line1;

    /**
     * @var string $line2
     */
    public $line2;

    /**
     * @var string $city
     */
    public $city;

    /**
     * @var string $country_code
     */
    public $country_code;

    /**
     * @var string $postal_code
     */
    public $postal_code;

    /**
     * @var  string $state
     */
    public $state;

    /**
     * @var string $phone
     */
    public $phone;

    /**
     * @var string $normalization_status
     */
    public $normalization_status;

    /**
     * @var string $status
     */
    public $status;

    /**
     * @var string $type
     */
    public $type;

    /**
     * @var string $receipt_name
     */
    public $receipt_name;
}