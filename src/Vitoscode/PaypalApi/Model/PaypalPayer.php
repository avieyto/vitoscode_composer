<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 9:15 AM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalPayer
{
    /**
     * @var string $payment_method
     */
    public $payment_method;

    /**
     * @var string $state
     */
    public $state;

    /**
     * @var array $funding_instruments
     */
    public $funding_instruments;

    /**
     * @var object $payer_info
     */
    public $payer_info;
}