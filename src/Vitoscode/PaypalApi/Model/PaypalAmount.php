<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:28 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalAmount
{
    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var float $total
     */
    public $total;

    /**
     * @var PaypalAmountDetails $details
     */
    public $details;

    public function __construct()
    {
        $this->currency = 'USD';
    }
}