<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 12:45 PM
 */

namespace Vitoscode\PaypalApi\Model;


class PaypalPaymentOptions
{
    /**
     * @var string $allowed_payment_method
     */
    public $allowed_payment_method;
}