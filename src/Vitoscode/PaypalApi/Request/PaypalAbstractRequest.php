<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 9:10 AM
 */

namespace Vitoscode\PaypalApi\Request;


class PaypalAbstractRequest
{
    /**
     * @return string
     */
    public function encodeToJson()
    {
        $tempEncode = json_encode($this);
        $tempDecodeArray = json_decode($tempEncode, true);
        $tempDecodeArray = $this->removeNulls($tempDecodeArray);
        return json_encode($tempDecodeArray);
    }

    private function removeNulls($array)
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $newValue = $this->removeNulls($value);
                if (count($newValue) > 0)
                    $newArray[$key] = $newValue;
            } else if ($value != null)
                $newArray[$key] = $value;
        }
        return $newArray;
    }
}