<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/23/2018
 * Time: 2:47 AM
 */

namespace Vitoscode\PaypalApi\Request;


use Vitoscode\PaypalApi\Model\PaypalAmount;
use Vitoscode\PaypalApi\Model\PaypalLink;
use Vitoscode\PaypalApi\Model\PaypalTransactionFee;

class CapturePaymentRequest extends PaypalAbstractRequest
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var PaypalAmount $amount
     */
    public $amount;

    /**
     * @var boolean $is_final_capture
     */
    public $is_final_capture;

    /**
     * @var string $state
     */
    public $state;

    /**
     * @var string $reason_code
     */
    public $reason_code;

    /**
     * @var string $parent_payment
     */
    public $parent_payment;

    /**
     * @var string $invoice_number
     */
    public $invoice_number;

    /**
     * @var PaypalTransactionFee $transaction_fee
     */
    public $transaction_fee;

    /**
     * @var string $create_time
     */
    public $create_time;

    /**
     * @var string $update_time
     */
    public $update_time;

    /**
     * @var PaypalLink[] $links
     */
    public $links;


}