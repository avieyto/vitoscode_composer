<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/17/2018
 * Time: 9:11 AM
 */

namespace Vitoscode\PaypalApi\Request;


use Vitoscode\PaypalApi\Model\PaypalAmount;
use Vitoscode\PaypalApi\Model\PaypalPayer;
use Vitoscode\PaypalApi\Model\PaypalRedirectUrls;
use Vitoscode\PaypalApi\Model\PaypalTransaction;

class CreatePaymentRequest extends PaypalAbstractRequest
{
    /**
     * @var string $intent
     */
    public $intent;

    /**
     * @var string $experience_profile_id
     */
    public $experience_profile_id;

    /**
     * @var PaypalPayer $payer
     */
    public $payer;

    /**
     * @var PaypalTransaction[] $transactions
     */
    public $transactions;

    /**
     * @var PaypalRedirectUrls $redirect_urls
     */
    public $redirect_urls;

    /**
     * CreatePaymentRequest constructor.
     */
    public function __construct()
    {
        $this->intent = 'sale';
        $this->transactions = array();
    }

    /**
     * @param $url
     * @return $this
     */
    public function setReturnUrl($url)
    {
        if (!$this->redirect_urls)
            $this->redirect_urls = new PaypalRedirectUrls();
        $this->redirect_urls->return_url = $url;
        return $this;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setCancelUrl($url)
    {
        if (!$this->redirect_urls)
            $this->redirect_urls = new PaypalRedirectUrls();
        $this->redirect_urls->cancel_url = $url;
        return $this;
    }

    /**
     * @param PaypalTransaction $transaction
     * @return $this
     */
    public function addTransaction(PaypalTransaction $transaction)
    {
        if (!$this->transactions)
            $this->transactions = array();
        $this->transactions[] = $transaction;
        return $this;
    }

    /**
     * @param $amount
     * @param $description
     * @return CreatePaymentRequest
     */
    public function addSimpleTransaction($amount, $description, $currency)
    {
        $transaction = new PaypalTransaction();
        $transaction->amount = new PaypalAmount();
        $transaction->amount->total = $amount;
        $transaction->amount->currency = $currency;
        $transaction->description = $description;
        return $this->addTransaction($transaction);
    }
}