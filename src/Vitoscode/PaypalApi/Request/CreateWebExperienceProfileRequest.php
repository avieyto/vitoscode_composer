<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/2/2019
 * Time: 8:41 AM
 */

namespace Vitoscode\PaypalApi\Request;


class CreateWebExperienceProfileRequest extends PaypalAbstractRequest
{
    /**
     * @var string $name
     */
    public $name;

    /**
     * @var boolean $temporary
     */
    public $temporary;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalFlowConfig $flow_config
     */
    public $flow_config;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalInputFields $input_fields
     */
    public $input_fields;

    /**
     * @var \Vitoscode\PaypalApi\Model\PaypalPresentation $presentation
     */
    public $presentation;
}