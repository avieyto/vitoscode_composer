<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 6/15/2018
 * Time: 9:19 AM
 */

namespace Vitoscode\PaypalApi\Request;


use Vitoscode\PaypalApi\Model\PaypalTransaction;

class ExecutePaymentRequest extends PaypalAbstractRequest
{
    /**
     * @var string $payer_id
     */
    public $payer_id;

    /**
     * @var PaypalTransaction[] $transactions
     */
    public $transactions;
}