<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 1/26/2018
 * Time: 12:28 AM
 */

namespace Vitoscode\PaypalApi\Exception;

use Throwable;

class PaypalException extends \Exception implements IPaypalException
{
    /**
     * @var string $paypalResponse
     */
    public $paypalResponse;

    public function __construct($message = "", $code = 0, Throwable $previous = null, $paypalResponse = null)
    {
        parent::__construct($message, $code, $previous);
        $this->paypalResponse = $paypalResponse;
    }
}