<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 1/7/2019
 * Time: 12:00 PM
 */

namespace Vitoscode\Sample;

use Symfony\Component\EventDispatcher\EventDispatcher;

class SampleClass
{
    /**
     * @var integer
     */
    protected $number;

    /**
     * SampleClass constructor.
     * @param int $number
     */
    public function __construct($number = 0)
    {
        $this->number = $number;

        $dispatcher = new EventDispatcher();
    }

    /**
     * print the number
     */
    public function printNumber()
    {
        print_r($this->number * 2);
    }
}