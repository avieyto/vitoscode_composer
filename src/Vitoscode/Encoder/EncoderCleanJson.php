<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 5/23/2019
 * Time: 3:33 PM
 */

namespace Vitoscode\Encoder;


class EncoderCleanJson
{
    /**
     * @param object $object
     * @return false|string
     */
    public static function encodeCleanToJson($object)
    {
        $tempEncode = json_encode($object);
        $tempDecodeAsArray = json_decode($tempEncode, true);
        $tempDecodeAsArray = self::removeNulls($tempDecodeAsArray);
        return json_encode($tempDecodeAsArray);
    }

    /**
     * @param array $array
     * @return array
     */
    public static function removeNulls($array)
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $newValue = self::removeNulls($value);
                if (count($newValue) > 0)
                    $newArray[$key] = $newValue;
            } else if ($value != null)
                $newArray[$key] = $value;
        }
        return $newArray;
    }
}