<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/13/2018
 * Time: 8:14 PM
 */

namespace Vitoscode\OAuthLibrary;


interface IRegisterCustomerModel
{
    /**
     * @return string
     */
    function getAccountType();

    /**
     * @return string
     */
    function getPassword();

    /**
     * @return string
     */
    function getUsername();
}