<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 10:24 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

class AfterCheckTokenSignature extends BeforeCheckTokenSignature
{
    /**
     * @var IOAuthClient $client
     */
    protected $client;

    /**
     * @var IOAuthToken $token
     */
    protected $token;

    /**
     * @var boolean $validationResult
     */
    protected $validationResult;

    /**
     * AfterCheckTokenSignature constructor.
     * @param IOAuthSignature $signature
     * @param IOAuthClient $client
     * @param IOAuthToken $token
     * @param boolean $validationResult
     */
    public function __construct(IOAuthSignature $signature, IOAuthClient $client, IOAuthToken $token, $validationResult)
    {
        parent::__construct($signature);
        $this->client = $client;
        $this->token = $token;
        $this->validationResult = $validationResult;
    }

    /**
     * @param IOAuthClient $client
     * @return $this
     */
    public function setClient(IOAuthClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return IOAuthClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param IOAuthToken $token
     * @return $this
     */
    public function setToken(IOAuthToken $token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return IOAuthToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param boolean $validationResult
     * @return $this
     */
    public function setValidationResult($validationResult)
    {
        $this->validationResult = $validationResult;
        return $this;
    }

    /**
     * @return bool
     */
    public function getValidationResult()
    {
        return $this->validationResult;
    }

}