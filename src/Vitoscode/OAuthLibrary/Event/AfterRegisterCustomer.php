<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 9:24 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\IRegisterCustomerModel;
use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;

class AfterRegisterCustomer extends BeforeRegisterCustomer
{
    /**
     * @var IOAuthAccount $accountCreated
     */
    protected $accountCreated;

    public function __construct(IRegisterCustomerModel $registerCustomer, IOAuthSignature $signature, IOAuthAccount $accountCreated)
    {
        parent::__construct($registerCustomer, $signature);
        $this->accountCreated = $accountCreated;
    }

    /**
     * @param IOAuthAccount $accountCreated
     * @return $this
     */
    public function setAccountCreated(IOAuthAccount $accountCreated)
    {
        $this->accountCreated = $accountCreated;
        return $this;
    }

    /**
     * @return IOAuthAccount
     */
    public function getAccountCreated()
    {
        return $this->accountCreated;
    }
}