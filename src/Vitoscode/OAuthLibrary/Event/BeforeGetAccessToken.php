<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 9:33 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;

class BeforeGetAccessToken extends Event
{
    /**
     * @var string $userId
     */
    protected $userId;

    /**
     * @var IOAuthSignature $signature
     */
    protected $signature;

    /**
     * BeforeGetAccessToken constructor.
     * @param string $userId
     * @param IOAuthSignature $signature
     */
    public function __construct($userId, IOAuthSignature $signature)
    {
        $this->userId = $userId;
        $this->signature = $signature;
    }

    /**
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param IOAuthSignature $signature
     * @return $this
     */
    public function setSignature(IOAuthSignature $signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return IOAuthSignature
     */
    public function getSignature()
    {
        return $this->signature;
    }
}