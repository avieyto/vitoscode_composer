<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/23/2019
 * Time: 11:15 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


class Events
{
    CONST OAUTH_BEFORE_REGISTER_CUSTOMER = 'oauth_before_register_customer';
    CONST OAUTH_AFTER_REGISTER_CUSTOMER = 'oauth_after_register_customer';
    CONST OAUTH_BEFORE_GET_ACCESS_TOKEN = 'oauth_before_get_access_token';
    CONST OAUTH_AFTER_GET_ACCESS_TOKEN = 'oauth_after_get_access_token';
    CONST OAUTH_BEFORE_GET_TEMPORARY_TOKEN = 'oauth_before_get_temporary_token';
    CONST OAUTH_AFTER_GET_TEMPORARY_TOKEN = 'oauth_after_get_temporary_token';
    CONST OAUTH_BEFORE_GENERATE_CLIENT = 'oauth_before_generate_client';
    CONST OAUTH_AFTER_GENERATE_CLIENT = 'oauth_after_generate_client';
    CONST OAUTH_BEFORE_REFRESH_TOKEN = 'oauth_before_refresh_token';
    CONST OAUTH_AFTER_REFRESH_TOKEN = 'oauth_after_refresh_token';
    CONST OAUTH_BEFORE_CHECK_TOKEN_SIGNATURE = 'oauth_before_check_token_signature';
    CONST OAUTH_AFTER_CHECK_TOKEN_SIGNATURE = 'oauth_after_check_token_signature';
    CONST OAUTH_BEFORE_VALIDATE_TOKEN = 'oauth_before_validate_token';
    CONST OAUTH_AFTER_VALIDATE_TOKEN = 'oauth_after_validate_token';
}
