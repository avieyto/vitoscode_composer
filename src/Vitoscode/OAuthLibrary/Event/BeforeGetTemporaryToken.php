<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 9:33 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;

class BeforeGetTemporaryToken extends Event
{
    /**
     * @var IOAuthSignature $signature
     */
    protected $signature;

    /**
     * BeforeGetAccessToken constructor.
     * @param IOAuthSignature $signature
     */
    public function __construct(IOAuthSignature $signature)
    {
        $this->signature = $signature;
    }

    /**
     * @param IOAuthSignature $signature
     * @return $this
     */
    public function setSignature(IOAuthSignature $signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return IOAuthSignature
     */
    public function getSignature()
    {
        return $this->signature;
    }
}