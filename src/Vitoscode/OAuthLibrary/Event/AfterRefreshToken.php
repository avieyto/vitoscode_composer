<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 1:49 PM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

class AfterRefreshToken extends BeforeRefreshToken
{
    /**
     * @var IOAuthClient $client
     */
    protected $client;

    /**
     * @var IOAuthToken $refreshedToken
     */
    protected $refreshedToken;

    public function __construct($refreshToken, IOAuthSignature $signature, IOAuthClient $client, IOAuthToken $refreshedToken)
    {
        parent::__construct($refreshToken, $signature);
        $this->client = $client;
        $this->refreshedToken = $refreshedToken;
    }

    /**
     * @param IOAuthClient $client
     * @return $this
     */
    public function setClient(IOAuthClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return IOAuthClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param IOAuthToken $refreshedToken
     * @return $this
     */
    public function setRefreshedToken(IOAuthToken $refreshedToken)
    {
        $this->refreshedToken = $refreshedToken;
        return $this;
    }

    /**
     * @return IOAuthToken
     */
    public function getRefreshedToken()
    {
        return $this->refreshedToken;
    }
}