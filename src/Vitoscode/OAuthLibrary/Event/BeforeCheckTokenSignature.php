<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 10:23 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;

class BeforeCheckTokenSignature extends Event
{
    /**
     * @var IOAuthSignature $signature
     */
    protected $signature;

    public function __construct(IOAuthSignature $signature)
    {
        $this->signature = $signature;
    }

    /**
     * @param IOAuthSignature $signature
     * @return $this
     */
    public function setSignature(IOAuthSignature $signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return IOAuthSignature
     */
    public function getSignature()
    {
        return $this->signature;
    }
}