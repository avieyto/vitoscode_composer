<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 11:31 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\Model\IOAuthClient;

class AfterGenerateClient extends BeforeGenerateClient
{
    /**
     * @var IOAuthClient
     */
    protected $client;

    /**
     * AfterGenerateClient constructor.
     * @param string $name
     * @param string $clientType
     * @param string $url
     * @param IOAuthClient $client
     */
    public function __construct($name, $clientType, $url, IOAuthClient $client)
    {
        parent::__construct($name, $clientType, $url);
        $this->client = $client;
    }

    /**
     * @param IOAuthClient $client
     * @return $this
     */
    public function setClient(IOAuthClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return IOAuthClient
     */
    public function getClient()
    {
        return $this->client;
    }
}