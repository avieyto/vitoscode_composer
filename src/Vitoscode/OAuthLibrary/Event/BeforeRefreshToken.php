<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 11:50 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;

class BeforeRefreshToken extends Event
{
    /**
     * @var string $refreshToken
     */
    protected $refreshToken;

    /**
     * @var IOAuthSignature $signature
     */
    protected $signature;

    public function __construct($refreshToken, IOAuthSignature $signature)
    {
        $this->refreshToken = $refreshToken;
        $this->signature = $signature;
    }

    /**
     * @param string $refreshToken
     * @return $this
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param IOAuthSignature $signature
     * @return $this
     */
    public function setSignature(IOAuthSignature $signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return IOAuthSignature
     */
    public function getSignature()
    {
        return $this->signature;
    }
}