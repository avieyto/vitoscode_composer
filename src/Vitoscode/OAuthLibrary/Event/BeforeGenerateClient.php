<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 11:27 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;

class BeforeGenerateClient extends Event
{
    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $clientType
     */
    protected $clientType;

    /**
     * @var string $url
     */
    protected $url;

    /**
     * BeforeGenerateClient constructor.
     * @param string $name
     * @param string $clientType
     * @param string $url
     */
    public function __construct($name, $clientType, $url)
    {
        $this->name = $name;
        $this->clientType = $clientType;
        $this->url = $url;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $clientType
     * @return $this
     */
    public function setClientType($clientType)
    {
        $this->clientType = $clientType;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}