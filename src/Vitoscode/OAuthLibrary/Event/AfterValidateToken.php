<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/16/2019
 * Time: 9:32 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

class AfterValidateToken extends BeforeValidateToken
{
    /**
     * @var boolean $validationResult
     */
    protected $validationResult;

    /**
     * @var IOAuthClient $client
     */
    protected $client;

    /**
     * @var IOAuthAccount $account
     */
    protected $account;

    /**
     * AfterValidateToken constructor.
     * @param IOAuthToken $token
     * @param IOAuthClient $client
     * @param IOAuthAccount $account
     * @param boolean $validationResult
     */
    public function __construct(IOAuthToken $token, IOAuthClient $client, IOAuthAccount $account, $validationResult)
    {
        parent::__construct($token);
        $this->validationResult = $validationResult;
        $this->client = $client;
        $this->account = $account;
    }

    /**
     * @param IOAuthClient $client
     * @return $this
     */
    public function setClient(IOAuthClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return IOAuthClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param boolean $validationResult
     * @return $this
     */
    public function setValidationResult($validationResult)
    {
        $this->validationResult = $validationResult;
        return $this;
    }

    /**
     * @return bool
     */
    public function getValidationResult()
    {
        return $this->validationResult;
    }

    /**
     * @param IOAuthAccount $account
     * @return $this
     */
    public function setAccount(IOAuthAccount $account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return IOAuthAccount
     */
    public function getAccount()
    {
        return $this->account;
    }
}
