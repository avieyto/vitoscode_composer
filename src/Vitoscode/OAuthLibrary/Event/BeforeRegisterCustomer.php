<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 9:03 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\OAuthLibrary\IRegisterCustomerModel;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;

class BeforeRegisterCustomer extends Event
{
    /**
     * @var IRegisterCustomerModel
     */
    protected $registerCustomer;

    /**
     * @var IOAuthSignature
     */
    protected $signature;

    public function __construct(IRegisterCustomerModel $registerCustomer, IOAuthSignature $signature)
    {
        $this->registerCustomer = $registerCustomer;
        $this->signature = $signature;
    }

    /**
     * @param IRegisterCustomerModel $registerCustomer
     * @return $this
     */
    public function setRegisterCustomer(IRegisterCustomerModel $registerCustomer)
    {
        $this->registerCustomer = $registerCustomer;
        return $this;
    }

    /**
     * @return IRegisterCustomerModel
     */
    public function getRegisterCustomer()
    {
        return $this->registerCustomer;
    }

    /**
     * @param IOAuthSignature $signature
     * @return $this
     */
    public function setSignature(IOAuthSignature $signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return IOAuthSignature
     */
    public function getSignature()
    {
        return $this->signature;
    }
}