<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 9:46 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

class AfterGetAccessToken extends BeforeGetAccessToken
{
    /**
     * @var IOAuthClient $client
     */
    protected $client;

    /**
     * @var IOAuthAccount $account
     */
    protected $account;

    /**
     * @var IOAuthToken $token
     */
    protected $token;

    public function __construct($userId, IOAuthSignature $signature, IOAuthClient $client, IOAuthAccount $account, IOAuthToken $token)
    {
        parent::__construct($userId, $signature);
        $this->client = $client;
        $this->account = $account;
        $this->token = $token;
    }

    /**
     * @param IOAuthClient $client
     * @return $this
     */
    public function setClient(IOAuthClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return IOAuthClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param IOAuthAccount $account
     * @return $this
     */
    public function setAccount(IOAuthAccount $account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return IOAuthAccount
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param IOAuthToken $token
     * @return $this
     */
    public function setToken(IOAuthToken $token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return IOAuthToken
     */
    public function getToken()
    {
        return $this->token;
    }
}