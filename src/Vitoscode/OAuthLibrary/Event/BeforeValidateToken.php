<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 10:23 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

class BeforeValidateToken extends Event
{
    /**
     * @var IOAuthToken $token
     */
    protected $token;

    public function __construct(IOAuthToken $token)
    {
        $this->token = $token;
    }

    /**
     * @param IOAuthToken $token
     * @return $this
     */
    public function setToken(IOAuthToken $token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return IOAuthToken
     */
    public function getToken()
    {
        return $this->token;
    }
}
