<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/25/2019
 * Time: 9:46 AM
 */

namespace Vitoscode\OAuthLibrary\Event;


use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

class AfterGetTemporaryToken extends BeforeGetTemporaryToken
{
    /**
     * @var IOAuthClient $client
     */
    protected $client;

    /**
     * @var IOAuthToken $token
     */
    protected $token;

    public function __construct(IOAuthSignature $signature, IOAuthClient $client, IOAuthToken $token)
    {
        parent::__construct($signature);
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * @param IOAuthClient $client
     * @return $this
     */
    public function setClient(IOAuthClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return IOAuthClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param IOAuthToken $token
     * @return $this
     */
    public function setToken(IOAuthToken $token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return IOAuthToken
     */
    public function getToken()
    {
        return $this->token;
    }
}