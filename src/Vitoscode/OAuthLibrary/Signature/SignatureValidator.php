<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/11/2018
 * Time: 8:59 PM
 */

namespace Vitoscode\OAuthLibrary\Signature;


use Vitoscode\OAuthLibrary\Exception\OAuthException;
use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Signature\Encoder\ISignatureEncoder;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;
use Vitoscode\Util\DateTime\DateTimeHelper;

class SignatureValidator implements ISignatureValidator
{
    /**
     * @var ISignatureEncoder
     */
    protected $encoder;

    public function __construct(ISignatureEncoder $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param IOAuthSignature $signature
     * @param IOAuthClient $client
     * @param IOAuthAccount $account
     * @param IOAuthToken $token
     * @return bool
     * @throws OAuthException
     * @throws \Exception
     */
    public function validateSignature(IOAuthSignature $signature, IOAuthClient $client, IOAuthAccount $account = null, IOAuthToken $token = null)
    {
        //check datetime
        $currentDateTime = DateTimeHelper::getDateTime();
        $nonceDateTime = $signature->getDateTime();
        if ($nonceDateTime != null) {
            $interval = $currentDateTime->diff($nonceDateTime);
            if ($interval->i > 5)
                throw new OAuthException('Invalid datetime, datetime should be less that 5 minutes of the current datetime', OAuthException::EXCEPTION_INVALID_DATETIME);
        }
        if ($token && $token->isExpired())
            throw new OAuthException('Token is expired, please refresh it or create a new one');
        $clientSecret = ($client)?$client->getClientSecret():null;
        $accountSecret = ($account)?$account->getAccountSecret():null;
        $tokenSecret = ($token)?$token->getTokenSecret():null;
        $signatureReGenerated = $this->encoder->encode($signature->getNonce(), $signature->getDateTime(), $clientSecret, $tokenSecret, $accountSecret);
        return ($signatureReGenerated === $signature->getSignature());
    }
}
