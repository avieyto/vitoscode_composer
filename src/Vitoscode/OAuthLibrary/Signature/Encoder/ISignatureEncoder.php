<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/13/2018
 * Time: 8:35 PM
 */

namespace Vitoscode\OAuthLibrary\Signature\Encoder;

use DateTime;

interface ISignatureEncoder
{
    /**
     * @param string $nonce
     * @return $this
     */
    function setNonce(string $nonce) : self;

    /**
     * @param DateTime $dateTime
     * @return $this
     */
    function setDateTime(\DateTime $dateTime) : self;

    /**
     * @param string $clientSecret
     * @return $this
     */
    function setClientSecret(string $clientSecret) : self;

    /**
     * @param string $tokenSecret
     * @return $this
     */
    function setTokenSecret(string $tokenSecret) : self;

    /**
     * @param string $accountSecret
     * @return $this
     */
    function setAccountSecret(string $accountSecret) : self;

    /**
     * Build the signature phrase from nonce, dateTime, clientSecret, tokenSecret and accountSecret
     * @return mixed
     */
    function build();

    /**
     * Encode the signature
     * @return string
     */
    function encode() : string;
}
