<?php


namespace Vitoscode\OAuthLibrary\Signature\Encoder;

use DateTime;

/**
 * Class AbstractSignatureEncoder
 * @package Vitoscode\OAuthLibrary\Signature\Encoder
 */
abstract class AbstractSignatureEncoder implements ISignatureEncoder
{
    /**
     * @var string $nonce
     */
    protected $nonce;

    /**
     * @var string $dateTime
     */
    protected $dateTime;

    /**
     * @var string $clientSecret
     */
    protected $clientSecret;

    /**
     * @var string $tokenSecret
     */
    protected $tokenSecret;

    /**
     * @var string $accountSecret
     */
    protected $accountSecret;

    /**
     * @param string $nonce
     * @return ISignatureEncoder
     */
    public function setNonce(string $nonce): ISignatureEncoder
    {
        $this->nonce = $nonce;
        return $this;
    }

    /**
     * @param DateTime $dateTime
     * @return ISignatureEncoder
     */
    public function setDateTime(DateTime $dateTime): ISignatureEncoder
    {
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * @param string $clientSecret
     * @return ISignatureEncoder
     */
    public function setClientSecret(string $clientSecret): ISignatureEncoder
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    /**
     * @param string $tokenSecret
     * @return ISignatureEncoder
     */
    public function setTokenSecret(string $tokenSecret): ISignatureEncoder
    {
        $this->tokenSecret = $tokenSecret;
        return $this;
    }

    /**
     * @param string $accountSecret
     * @return ISignatureEncoder
     */
    public function setAccountSecret(string $accountSecret): ISignatureEncoder
    {
        $this->accountSecret = $accountSecret;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function build()
    {
        $pieces[] = $this->nonce;
        if ($this->dateTime)
            $pieces[] = $this->dateTime->format('Y-m-d H:i:s');
        if ($this->clientSecret)
            $pieces[] = $this->clientSecret;
        if ($this->tokenSecret)
            $pieces[] = $this->tokenSecret;
        if ($this->accountSecret)
            $pieces[] = $this->accountSecret;
        return implode('', $pieces);
    }

    /**
     * @inheritDoc
     */
    public function encode() : string
    {
        return $this->build();
    }
}
