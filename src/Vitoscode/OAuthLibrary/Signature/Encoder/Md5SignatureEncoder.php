<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/11/2018
 * Time: 8:08 PM
 */

namespace Vitoscode\OAuthLibrary\Signature\Encoder;


class Md5SignatureEncoder extends AbstractSignatureEncoder
{
    /**
     * @inheritDoc
     */
    public function encode() : string
    {
        $signaturePhrase = $this->build();
        return md5($signaturePhrase);
    }
}
