<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/11/2018
 * Time: 8:08 PM
 */

namespace Vitoscode\OAuthLibrary\Signature\Encoder;


class Sha1SignatureEncoder extends AbstractSignatureEncoder
{
    public function __construct()
    {

    }

    /**
     * @inheritDoc
     */
    public function encode($nonce, $dateTime, $clientSecret = null, $tokenSecret = null, $accountSecret = null)
    {
        $signaturePhrase = $this->build($nonce, $dateTime, $clientSecret, $tokenSecret, $accountSecret);
        return sha1($signaturePhrase);
    }
}
