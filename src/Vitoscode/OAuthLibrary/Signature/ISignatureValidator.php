<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/13/2018
 * Time: 8:32 PM
 */

namespace Vitoscode\OAuthLibrary\Signature;


use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;


interface ISignatureValidator
{
    /**
     * @param IOAuthSignature $signature
     * @param IOAuthClient $client
     * @param IOAuthAccount $account
     * @param IOAuthToken $token
     * @return bool
     */
    function validateSignature(IOAuthSignature $signature, IOAuthClient $client, IOAuthAccount $account = null, IOAuthToken $token = null);
}