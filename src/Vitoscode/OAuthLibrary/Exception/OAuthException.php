<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/9/2018
 * Time: 12:54 PM
 */

namespace Vitoscode\OAuthLibrary\Exception;


use Throwable;

class OAuthException extends \Exception
{
    CONST EXCEPTION_INVALID_SIGNATURE = 1000;
    CONST EXCEPTION_INVALID_DATETIME = 1001;
    CONST EXCEPTION_INVALID_NONCE = 1002;
    CONST EXCEPTION_FAIL_TOKEN_CREATION = 1003;
    CONST EXCEPTION_FAIL_TOKEN_REFRESH = 1004;

    CONST EXCEPTION_NOT_FOUND = 1404;

    /**
     * OAuthException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}