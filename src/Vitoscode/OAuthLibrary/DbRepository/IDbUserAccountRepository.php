<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/9/2018
 * Time: 12:26 PM
 */

namespace Vitoscode\OAuthLibrary\DbRepository;


use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\IRegisterCustomerModel;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

interface IDbUserAccountRepository
{
    /**
     * @param IRegisterCustomerModel $customerModel
     * @param IOAuthClient $client
     * @return IOAuthAccount
     */
    function registerUserAccount(IRegisterCustomerModel $customerModel, IOAuthClient $client);

    /**
     * @param string $userId
     * @return IOAuthAccount|null
     */
    function getUserAccount($userId);

    /**
     * @param IOAuthToken $token
     * @return IOAuthAccount
     */
    function getUserAccountByToken(IOAuthToken $token);
}
