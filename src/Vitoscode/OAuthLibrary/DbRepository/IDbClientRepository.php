<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/9/2018
 * Time: 12:27 PM
 */

namespace Vitoscode\OAuthLibrary\DbRepository;


use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

interface IDbClientRepository
{
    /**
     * @param $clientId
     * @return IOAuthClient
     */
    function getClientById($clientId);

    /**
     * @param IOAuthClient $oauthClient
     * @return IOAuthClient
     */
    function storeClient(IOAuthClient $oauthClient);

    /**
     * @param IOAuthToken $token
     * @return IOAuthClient
     */
    function getClientByToken(IOAuthToken $token);
}
