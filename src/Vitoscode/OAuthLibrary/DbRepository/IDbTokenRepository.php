<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/9/2018
 * Time: 12:24 PM
 */

namespace Vitoscode\OAuthLibrary\DbRepository;


use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;

interface IDbTokenRepository
{
    /**
     * @param IOAuthToken $token
     * @param IOAuthClient $client
     * @param IOAuthAccount $account
     * @return mixed
     */
    function storeToken(IOAuthToken $token, IOAuthClient $client, IOAuthAccount $account = null);

    /**
     * @param IOAuthToken $token
     * @param IOAuthClient $client
     * @param IOAuthAccount|null $account
     * @return IOAuthToken
     */
    function updateToken(IOAuthToken $token, IOAuthClient $client, IOAuthAccount $account = null);

    /**
     * @param string $tokenId
     * @return IOAuthToken
     */
    function findToken($tokenId);
}