<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/13/2018
 * Time: 8:23 PM
 */

namespace Vitoscode\OAuthLibrary\DbRepository;


interface IDbRepository
{
    /**
     * @return IDbTokenRepository
     */
    function getTokenRepository();

    /**
     * @return IDbNonceRepository
     */
    function getNonceRepository();

    /**
     * @return IDbUserAccountRepository
     */
    function getUserAccountRepository();

    /**
     * @return IDbClientRepository
     */
    function getClientRepository();
}