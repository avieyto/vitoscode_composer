<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/9/2018
 * Time: 12:25 PM
 */

namespace Vitoscode\OAuthLibrary\DbRepository;


interface IDbNonceRepository
{
    /**
     * @param string $nonce
     * @return bool
     */
    function existNonce($nonce);

    /**
     * @param $nonce
     * @param \DateTime $dateTime
     * @return mixed
     */
    function addNonce($nonce, $dateTime);
}