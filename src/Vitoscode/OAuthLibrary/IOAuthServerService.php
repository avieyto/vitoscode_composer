<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/4/2018
 * Time: 2:07 PM
 */

namespace Vitoscode\OAuthLibrary;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Vitoscode\Commons\Dispatchable\IDispatchableInterface;
use Vitoscode\OAuthLibrary\DbRepository\IDbRepository;
use Vitoscode\OAuthLibrary\Exception\OAuthException;
use Vitoscode\OAuthLibrary\Model\IOAuthAccount;
use Vitoscode\OAuthLibrary\Model\IOAuthClient;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;
use Vitoscode\OAuthLibrary\Signature\ISignatureValidator;
use Vitoscode\Util\IdGenerator\IUniqueGenerator;


interface IOAuthServerService extends IDispatchableInterface
{
    /**
     * IOAuthService constructor.
     * @param IDbRepository $authDbRepository
     * @param ISignatureValidator $signatureValidator
     * @param IUniqueGenerator $idGenerator
     * @param EventDispatcherInterface $dispatcher
     */
    function __construct(IDbRepository $authDbRepository, ISignatureValidator $signatureValidator, IUniqueGenerator $idGenerator, EventDispatcherInterface $dispatcher = null);

    /**
     * Register new account
     * @param IRegisterCustomerModel $registerCustomer
     * @param IOAuthSignature $signature
     * @return IOAuthAccount
     * @throws OAuthException
     */
    function RegisterCustomer(IRegisterCustomerModel $registerCustomer, IOAuthSignature $signature);

    /**
     * @param $userLogin
     * @param IOAuthSignature $signature
     * @return IOAuthToken
     * @throws OAuthException
     */
    function GetAccessToken($userLogin, IOAuthSignature $signature);

    /**
     * @param string $refreshToken
     * @param IOAuthSignature $signature
     * @return IOAuthToken
     * @throws OAuthException
     */
    function RefreshToken($refreshToken, IOAuthSignature $signature);

    /**
     * @param IOAuthSignature $signature
     * @return IOAuthToken|boolean
     * @throws OAuthException
     */
    function CheckTokenSignature(IOAuthSignature $signature);

    /**
     * Return the account if token is valid, otherwise false
     * @param IOAuthToken $token
     * @return IOAuthAccount
     * @throws OAuthException
     */
    function ValidateToken(IOAuthToken $token);

    /**
     * @param IOAuthSignature $signature
     * @return IOAuthToken
     * @throws OAuthException
     */
    function GetTemporaryToken(IOAuthSignature $signature);

    /**
     * @param string $name
     * @param string $clientType
     * @param string $url
     * @return IOAuthClient
     * @throws OAuthException
     */
    function GenerateClient($name, $clientType, $url = null);
}
