<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 4/17/2019
 * Time: 8:41 PM
 */

namespace Vitoscode\OAuthLibrary\Model;


class OAuthClient implements IOAuthClient
{
    const WEB_CLIENT_TYPE = 'WEB';
    const ANDROID_MOBILE_CLIENT_TYPE = 'ANDROID';
    const IPHONE_MOBILE_CLIENT_TYPE = 'IPHONE';

    /**
     * @var string $clientId
     */
    protected $clientId;

    /**
     * @var string $clientSecret
     */
    protected $clientSecret;

    /**
     * @var string $clientType
     */
    protected $clientType;

    /**
     * @var string $clientName
     */
    protected $clientName;

    public function __construct($clientId, $clientName, $clientSecret, $clientType = self::WEB_CLIENT_TYPE)
    {
        $this->clientType = $clientType;
        $this->clientSecret = $clientSecret;
        $this->clientId = $clientId;
        $this->clientName = $clientName;
    }

    /**
     * @return string
     */
    function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @return string
     */
    function getClientType()
    {
        return $this->clientType;
    }

    /**
     * @return string
     */
    function getClientName()
    {
        return $this->clientName;
    }
}