<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/11/2018
 * Time: 10:54 PM
 */

namespace Vitoscode\OAuthLibrary\Model;;


use Vitoscode\OAuthLibrary\Exception\OAuthException;

class OAuthSignature implements IOAuthSignature
{
    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $nonce;

    /**
     * @var \DateTime
     */
    protected $dateTime;

    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $tokenId;

    /**
     * Signature constructor.
     * @param $clientId
     * @param $nonce
     * @param $dateTime
     * @param $signature
     * @param $tokenId
     * @throws OAuthException
     */
    public function __construct($clientId, $nonce, $dateTime, $signature, $tokenId = null)
    {
        $this->clientId = $clientId;
        $this->nonce = $nonce;
        $this->dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
        if (!$this->dateTime)
            throw new OAuthException('Invalid datetime format '.$dateTime.' valid format Y-m-d H:i:s', OAuthException::EXCEPTION_INVALID_DATETIME);
        $this->signature = $signature;
        $this->tokenId = $tokenId;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * @return string
     */
    public function getTokenId()
    {
        return $this->tokenId;
    }
}