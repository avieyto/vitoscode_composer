<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/11/2018
 * Time: 8:22 PM
 */

namespace Vitoscode\OAuthLibrary\Model;


interface IOAuthClient
{
    /**
     * @return string
     */
    function getClientId();

    /**
     * @return string
     */
    function getClientSecret();

    /**
     * @return string
     */
    function getClientType();

    /**
     * @return string
     */
    function getClientName();
}