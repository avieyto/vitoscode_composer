<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 12/9/2018
 * Time: 12:52 PM
 */

namespace Vitoscode\OAuthLibrary\Model;


interface IOAuthAccount
{
    const STATUS_CREATED = 'created';
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    const ACCOUNT_TYPE_USER = 'user';
    const ACCOUNT_TYPE_ADMIN = 'admin';
    const ACCOUNT_TYPE_SUPERUSER = 'superuser';

    /**
     * @return string
     */
    function getAccountId();

    /**
     * @return string
     */
    function getAccountSecret();
}