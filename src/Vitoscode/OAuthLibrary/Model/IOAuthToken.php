<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/13/2018
 * Time: 7:22 PM
 */

namespace Vitoscode\OAuthLibrary\Model;;


interface IOAuthToken
{
    const TOKEN_TYPE_ACCESS = 'ACCESS_TOKEN';
    const TOKEN_TYPE_TEMPORARY = 'TEMPORARY_TOKEN';

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    /**
     * @return string
     */
    function getToken();

    /**
     * @return string
     */
    function getTokenSecret();

    /**
     * @return string
     */
    function getTokenType();

    /**
     * @return int
     */
    function getExpireIn();

    /**
     * @return string
     */
    function getRefreshToken();

    /**
     * @return \DateTime
     */
    function getCreatedAt();

    /**
     * @return boolean
     */
    function isExpired();
}
