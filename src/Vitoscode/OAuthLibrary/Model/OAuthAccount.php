<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 4/17/2019
 * Time: 8:37 PM
 */

namespace Vitoscode\OAuthLibrary\Model;


class OAuthAccount implements IOAuthAccount
{
    /**
     * @var string $accountId
     */
    protected $accountId;

    /**
     * @var string $accountSecret
     */
    protected $accountSecret;

    public function __construct($accountId, $accountSecret)
    {
        $this->accountId = $accountId;
        $this->accountSecret = $accountSecret;
    }

    /**
     * @return string
     */
    function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @return string
     */
    function getAccountSecret()
    {
        return $this->accountSecret;
    }
}