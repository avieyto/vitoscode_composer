<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/13/2018
 * Time: 8:14 PM
 */

namespace Vitoscode\OAuthLibrary\Model;;


interface IOAuthSignature
{
    /**
     * Get signature
     * @return string
     */
    function getSignature();

    /**
     * @return string
     */
    function getNonce();

    /**
     * @return \DateTime
     */
    function getDateTime();

    /**
     * @return string
     */
    function getClientId();

    /**
     * @return string
     */
    function getTokenId();
}