<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 4/17/2019
 * Time: 8:50 PM
 */

namespace Vitoscode\OAuthLibrary\Model;


use Vitoscode\Util\DateTime\DateTimeHelper;

class OAuthToken implements IOAuthToken
{
    /**
     * @var string $token
     */
    protected $token;

    /**
     * @var string $tokenSecret
     */
    protected $tokenSecret;

    /**
     * @var string $tokenType
     */
    protected $tokenType;

    /**
     * @var int $expireIn
     */
    protected $expireIn;

    /**
     * @var string $refreshToken
     */
    protected $refreshToken;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * OAuthToken constructor.
     * @param $token
     * @param $tokenSecret
     * @param $tokenType
     * @param $expireIn
     * @param $refreshToken
     * @param $createdAt
     */
    public function __construct($token, $tokenSecret, $tokenType, $expireIn, $refreshToken, $createdAt)
    {
        $this->token = $token;
        $this->tokenSecret = $tokenSecret;
        $this->tokenType = $tokenType;
        $this->expireIn = $expireIn;
        $this->refreshToken = $refreshToken;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getTokenSecret()
    {
        return $this->tokenSecret;
    }

    /**
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @return int
     */
    public function getExpireIn()
    {
        return $this->expireIn;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isExpired()
    {
        $now = DateTimeHelper::getDateTime();
        $interval = $now->diff($this->createdAt);
        return ($interval->i > $this->expireIn);
    }
}
