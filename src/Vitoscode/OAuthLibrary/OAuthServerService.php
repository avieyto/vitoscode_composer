<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 11/2/2018
 * Time: 10:47 AM
 */

namespace Vitoscode\OAuthLibrary;


use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Vitoscode\Commons\Dispatchable\IDispatchableInterface;
use Vitoscode\OAuthLibrary\DbRepository\IDbRepository;
use Vitoscode\OAuthLibrary\Event\AfterCheckTokenSignature;
use Vitoscode\OAuthLibrary\Event\AfterGenerateClient;
use Vitoscode\OAuthLibrary\Event\AfterGetAccessToken;
use Vitoscode\OAuthLibrary\Event\AfterGetTemporaryToken;
use Vitoscode\OAuthLibrary\Event\AfterRefreshToken;
use Vitoscode\OAuthLibrary\Event\AfterRegisterCustomer;
use Vitoscode\OAuthLibrary\Event\AfterValidateToken;
use Vitoscode\OAuthLibrary\Event\BeforeCheckTokenSignature;
use Vitoscode\OAuthLibrary\Event\BeforeGenerateClient;
use Vitoscode\OAuthLibrary\Event\BeforeGetAccessToken;
use Vitoscode\OAuthLibrary\Event\BeforeGetTemporaryToken;
use Vitoscode\OAuthLibrary\Event\BeforeRefreshToken;
use Vitoscode\OAuthLibrary\Event\BeforeRegisterCustomer;
use Vitoscode\OAuthLibrary\Event\BeforeValidateNonce;
use Vitoscode\OAuthLibrary\Event\BeforeValidateSignature;
use Vitoscode\OAuthLibrary\Event\BeforeValidateToken;
use Vitoscode\OAuthLibrary\Event\Events;
use Vitoscode\OAuthLibrary\Exception\OAuthException;
use Vitoscode\OAuthLibrary\Model\IOAuthSignature;
use Vitoscode\OAuthLibrary\Model\OAuthAccount;
use Vitoscode\OAuthLibrary\Model\OAuthClient;
use Vitoscode\OAuthLibrary\Model\OAuthToken;
use Vitoscode\OAuthLibrary\Signature\ISignatureValidator;
use Vitoscode\OAuthLibrary\Model\IOAuthToken;
use Vitoscode\Util\DateTime\DateTimeHelper;
use Vitoscode\Util\IdGenerator\IUniqueGenerator;
use Vitoscode\OAuthLibrary\Model\IOAuthAccount;

class OAuthServerService implements IOAuthServerService
{
    /**
     * @var IDbRepository $authDbRepository
     */
    protected $authDbRepository;

    /**
     * @var ISignatureValidator $signatureValidator
     */
    protected $signatureValidator;

    /**
     * @var IUniqueGenerator $idGenerator
     */
    protected $idGenerator;

    /**
     * @var EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    /**
     * OAuthService constructor.
     * @param IDbRepository $authDbRepository
     * @param ISignatureValidator $signatureValidator
     * @param IUniqueGenerator $idGenerator
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(IDbRepository $authDbRepository, ISignatureValidator $signatureValidator, IUniqueGenerator $idGenerator, EventDispatcherInterface $dispatcher = null)
    {
        $this->authDbRepository = $authDbRepository;
        $this->signatureValidator = $signatureValidator;
        $this->idGenerator = $idGenerator;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function RegisterCustomer(IRegisterCustomerModel $registerCustomer, IOAuthSignature $signature)
    {
        $eventBeforeRegisterCustomer = new BeforeRegisterCustomer($registerCustomer, $signature);
        $this->dispatchEvent(Events::OAUTH_BEFORE_REGISTER_CUSTOMER, $eventBeforeRegisterCustomer);
        $registerCustomer = $eventBeforeRegisterCustomer->getRegisterCustomer();
        $signature = $eventBeforeRegisterCustomer->getSignature();

        $signature = $this->validateNonce($signature);

        $client = $this->authDbRepository->getClientRepository()->getClientById($signature->getClientId());
        if (!$client)
            throw new OAuthException("Client Id ".$signature->getClientId().' not found', OAuthException::EXCEPTION_NOT_FOUND);

        if ($this->signatureValidator->validateSignature($signature, $client, null, null)) {
            /**
             * @var $oathAccount IOAuthAccount
             */
            $oathAccount = $this->authDbRepository->getUserAccountRepository()->registerUserAccount($registerCustomer, $client);
            $eventAfterRegisterCustomer = new AfterRegisterCustomer($registerCustomer, $signature, $oathAccount);
            $this->dispatchEvent(Events::OAUTH_AFTER_REGISTER_CUSTOMER, $eventAfterRegisterCustomer);
            return $eventAfterRegisterCustomer->getAccountCreated();
        }
        else
            throw new OAuthException("Invalid signature", OAuthException::EXCEPTION_INVALID_SIGNATURE);
    }

    /**
     * {@inheritdoc}
     */
    public function GetAccessToken($userId, IOAuthSignature $signature)
    {
        $eventBeforeGetAccessToken = new BeforeGetAccessToken($userId, $signature);
        $this->dispatchEvent(Events::OAUTH_BEFORE_GET_ACCESS_TOKEN, $eventBeforeGetAccessToken);
        $userId = $eventBeforeGetAccessToken->getUserId();
        $signature = $eventBeforeGetAccessToken->getSignature();

        $this->validateNonce($signature);
        $client = $this->authDbRepository->getClientRepository()->getClientById($signature->getClientId());
        if (!$client)
            throw new OAuthException("Client Id ".$signature->getClientId().' not found', OAuthException::EXCEPTION_NOT_FOUND);

        /**
         * @var $account IOAuthAccount
         */
        $account = $this->authDbRepository->getUserAccountRepository()->getUserAccount($userId);
        if (!$account)
            throw new OAuthException("User Id ".$userId." not found", OAuthException::EXCEPTION_NOT_FOUND);

        if ($this->signatureValidator->validateSignature($signature, $client, $account, null)) {
            $tokenId = $this->idGenerator->generateId();
            $tokenSecret = $this->idGenerator->generateSecret();
            $refreshToken = $this->idGenerator->generateId();
            $expireIn = 3600;
            $creationDate = DateTimeHelper::getDateTime();
            $token = new OAuthToken($tokenId, $tokenSecret, IOAuthToken::TOKEN_TYPE_ACCESS, $expireIn, $refreshToken, $creationDate);
            $token = $this->authDbRepository->getTokenRepository()->storeToken($token, $client, $account);

            $eventAfterGetAccessToken = new AfterGetAccessToken($userId, $signature, $client, $account, $token);
            $this->dispatchEvent(Events::OAUTH_AFTER_GET_ACCESS_TOKEN, $eventAfterGetAccessToken);

            if ($eventAfterGetAccessToken->getToken()) {
                return $eventAfterGetAccessToken->getToken();
            }
            throw new OAuthException("Failed storing the access_token for user id .".$userId, OAuthException::EXCEPTION_FAIL_TOKEN_CREATION);
        }
        else
            throw new OAuthException('Invalid signature', OAuthException::EXCEPTION_INVALID_SIGNATURE);
    }

    /**
     * {@inheritdoc}
     */
    public function GetTemporaryToken(IOAuthSignature $signature)
    {
        $eventBeforeGetTemporaryToken = new BeforeGetTemporaryToken($signature);
        $this->dispatchEvent(Events::OAUTH_BEFORE_GET_TEMPORARY_TOKEN, $eventBeforeGetTemporaryToken);
        $signature = $eventBeforeGetTemporaryToken->getSignature();

        $this->validateNonce($signature);
        $client = $this->authDbRepository->getClientRepository()->getClientById($signature->getClientId());
        if (!$client)
            throw new OAuthException("Client Id ".$signature->getClientId().' not found', OAuthException::EXCEPTION_NOT_FOUND);

        if ($this->signatureValidator->validateSignature($signature, $client, null, null)) {
            $tokenId = $this->idGenerator->generateId();
            $tokenSecret = $this->idGenerator->generateSecret();
            $refreshToken = $this->idGenerator->generateId();
            $expireIn = 3600;
            $creationDate = DateTimeHelper::getDateTime();
            $token = new OAuthToken($tokenId, $tokenSecret, IOAuthToken::TOKEN_TYPE_TEMPORARY, $expireIn, $refreshToken, $creationDate);
            $token = $this->authDbRepository->getTokenRepository()->storeToken($token, $client, null);

            $eventAfterGetTemporaryToken = new AfterGetTemporaryToken($signature, $client, $token);
            $this->dispatchEvent(Events::OAUTH_AFTER_GET_ACCESS_TOKEN, $eventAfterGetTemporaryToken);

            if ($eventAfterGetTemporaryToken->getToken()) {
                return $eventAfterGetTemporaryToken->getToken();
            }
            throw new OAuthException("Failed creating the temporary access_token", OAuthException::EXCEPTION_FAIL_TOKEN_CREATION);
        }
        else
            throw new OAuthException('Invalid signature', OAuthException::EXCEPTION_INVALID_SIGNATURE);
    }

    /**
     * {@inheritdoc}
     */
    public function CheckTokenSignature(IOAuthSignature $signature)
    {
        $eventBeforeCheckTokenSignature = new BeforeCheckTokenSignature($signature);
        $this->dispatchEvent(Events::OAUTH_BEFORE_CHECK_TOKEN_SIGNATURE, $eventBeforeCheckTokenSignature);
        $signature = $eventBeforeCheckTokenSignature->getSignature();

        $this->validateNonce($signature);
        $client = $this->authDbRepository->getClientRepository()->getClientById($signature->getClientId());
        if (!$client)
            throw new OAuthException("Client Id ".$signature->getClientId().' not found', OAuthException::EXCEPTION_NOT_FOUND);
        $token = $this->authDbRepository->getTokenRepository()->findToken($signature->getTokenId());
        if (!$token)
            throw new OAuthException('Token Id '.$signature->getTokenId().' not found', OAuthException::EXCEPTION_NOT_FOUND);

        $validSignature = $this->signatureValidator->validateSignature($signature, $client, null, $token);

        $validationResult = $validSignature ? $token : false;
        $eventAfterCheckTokenSignature = new AfterCheckTokenSignature($signature, $client, $token, $validationResult);
        $this->dispatchEvent(Events::OAUTH_AFTER_CHECK_TOKEN_SIGNATURE, $eventAfterCheckTokenSignature);

        if ($eventAfterCheckTokenSignature->getValidationResult()) {
            return $eventAfterCheckTokenSignature->getValidationResult();
        }
        else
            throw new OAuthException('Invalid signature', OAuthException::EXCEPTION_INVALID_SIGNATURE);
    }

    /**
     * {@inheritdoc}
     */
    public function GenerateClient($name, $clientType, $url = null)
    {
        $eventBeforeGenerateClient = new BeforeGenerateClient($name, $clientType, $url);
        $this->dispatchEvent(Events::OAUTH_BEFORE_GENERATE_CLIENT, $eventBeforeGenerateClient);
        $name = $eventBeforeGenerateClient->getName();
        $clientType = $eventBeforeGenerateClient->getClientType();
        $url = $eventBeforeGenerateClient->getUrl();

        $clientId = $this->idGenerator->generateId();
        $clientSecret = $this->idGenerator->generateSecret();
        $client = new OAuthClient($clientId, $name, $clientSecret, $clientType);
        $client = $this->authDbRepository->getClientRepository()->storeClient($client);

        $eventAfterGenerateClient = new AfterGenerateClient($name, $clientType, $url, $client);
        $this->dispatchEvent(Events::OAUTH_AFTER_GENERATE_CLIENT, $eventAfterGenerateClient);
        return $eventAfterGenerateClient->getClient();
    }

    /**
     * {@inheritdoc}
     */
    public function RefreshToken($refreshToken, IOAuthSignature $signature)
    {
        $eventBeforeRefreshToken = new BeforeRefreshToken($refreshToken, $signature);
        $this->dispatchEvent(Events::OAUTH_BEFORE_REFRESH_TOKEN, $eventBeforeRefreshToken);
        $refreshToken = $eventBeforeRefreshToken->getRefreshToken();
        $signature = $eventBeforeRefreshToken->getSignature();

        $this->validateNonce($signature);
        $client = $this->authDbRepository->getClientRepository()->getClientById($signature->getClientId());
        if (!$client)
            throw new OAuthException("Client Id ".$signature->getClientId().' not found', OAuthException::EXCEPTION_NOT_FOUND);
        $token = $this->authDbRepository->getTokenRepository()->findToken($signature->getTokenId());
        if (!$token)
            throw new OAuthException('Token Id '.$signature->getTokenId().' not found', OAuthException::EXCEPTION_NOT_FOUND);
        if ($this->signatureValidator->validateSignature($signature, $client, null, $token)) {
            if ($refreshToken === $token->getRefreshToken()) {
                $newTokenSecret = $this->idGenerator->generateSecret();
                $newRefreshToken = $this->idGenerator->generateId();
                $expireIn = 3600;
                $creationDate = DateTimeHelper::getDateTime();
                $newTokenRefreshed = new OAuthToken($token->getToken(), $newTokenSecret, $token->getTokenType(), $expireIn, $newRefreshToken, $creationDate);
                $newTokenRefreshed = $this->authDbRepository->getTokenRepository()->updateToken($newTokenRefreshed, $client, null);

                $eventAfterRefreshToken = new AfterRefreshToken($refreshToken, $signature, $client, $newTokenRefreshed);
                $this->dispatchEvent(Events::OAUTH_AFTER_REFRESH_TOKEN, $eventAfterRefreshToken);
                return $eventAfterRefreshToken->getRefreshedToken();
            }
            else
                throw new OAuthException('Invalid refresh access_token', OAuthException::EXCEPTION_FAIL_TOKEN_REFRESH);
        }
        else
            throw new OAuthException('Invalid signature', OAuthException::EXCEPTION_INVALID_SIGNATURE);
    }

    /**
     * @inheritdoc
     */
    public function ValidateToken(IOAuthToken $token)
    {
        $eventBeforeValidateToken = new BeforeValidateToken($token);
        $this->dispatchEvent(Events::OAUTH_BEFORE_VALIDATE_TOKEN, $eventBeforeValidateToken);
        $token = $eventBeforeValidateToken->getToken();

        $retrievedToken = $this->authDbRepository->getTokenRepository()->findToken($token->getToken());
        if ($retrievedToken && !$retrievedToken->isExpired()) {
            $client = $this->authDbRepository->getClientRepository()->getClientByToken($retrievedToken);
            $account = $this->authDbRepository->getUserAccountRepository()->getUserAccountByToken($retrievedToken);
            $eventAfterValidateToken = new AfterValidateToken($retrievedToken, $client, $account, true);
        }
        else {
            $client = $this->authDbRepository->getClientRepository()->getClientByToken($retrievedToken);
            $account = $this->authDbRepository->getUserAccountRepository()->getUserAccountByToken($retrievedToken);
            $eventAfterValidateToken = new AfterValidateToken($retrievedToken, $client, $account, false);
        }

        $this->dispatchEvent(Events::OAUTH_AFTER_VALIDATE_TOKEN, $eventAfterValidateToken);
        return $eventAfterValidateToken->getAccount();
    }

    /**
     * {@inheritdoc}
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDispatcher()
    {
        return $this->dispatcher;
    }

    /**
     * @inheritdoc
     */
    public function dispatchEvent($eventName, Event $eventObject)
    {
        if ($this->dispatcher)
            $this->dispatcher->dispatch($eventName, $eventObject);
    }

    /**
     * @param IOAuthSignature $signature
     * @param string $method
     * @return IOAuthSignature
     * @throws OAuthException
     */
    protected function validateNonce(IOAuthSignature $signature)
    {
        $existNonce = $this->authDbRepository->getNonceRepository()->existNonce($signature->getNonce());
        if ($existNonce || $signature->getNonce() === null || $signature->getNonce() === "" || $signature->getNonce() === false)
            throw new OAuthException('Invalid nonce '.$signature->getNonce(), OAuthException::EXCEPTION_INVALID_NONCE);
        else
            $this->authDbRepository->getNonceRepository()->addNonce($signature->getNonce(), $signature->getDateTime());
        return $signature;
    }
}
