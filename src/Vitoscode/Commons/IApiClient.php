<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/19/2019
 * Time: 2:27 PM
 */

namespace Vitoscode\Commons;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Vitoscode\Commons\Dispatchable\IDispatchableInterface;
use Vitoscode\Commons\Environment\IApiEnvironment;

interface IApiClient extends IDispatchableInterface
{
    CONST POST = 'POST';
    CONST GET = 'GET';
    CONST PATCH = 'PATCH';

    /**
     * IApiClient constructor.
     * @param $apiName
     * @param IApiEnvironment $apiEnvironments
     */
    function __construct($apiName, IApiEnvironment $apiEnvironments);

    /**
     * @return string
     */
    function getApiName();

    /**
     * @return IApiEnvironment
     */
    function getApiEnvironments();

    /**
     * @param IApiEnvironment $apiEnvironments
     * @return $this
     */
    function setApiEnvironments(IApiEnvironment $apiEnvironments);

    /**
     * @param string $methodName
     * @param string $requestType (GET, POST, PUT, DELETE)
     * @param string $url
     * @param array $options
     * @param mixed $context
     * @return mixed
     */
    function request($methodName, $requestType, $url = '', array $options = [], $context = null);
}