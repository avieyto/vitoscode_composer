<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/26/2019
 * Time: 11:27 AM
 */

namespace Vitoscode\Commons\Environment;


class ApiEnvironment implements IApiEnvironment
{
    /**
     * @var array $environments
     */
    protected $environments;

    /**
     * ApiEnvironment constructor.
     * @param array $environmentsConfiguration
     * @throws \Exception
     */
    public function __construct(array $environmentsConfiguration)
    {
        $this->setEnvironmentsConfiguration($environmentsConfiguration);
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function setEnvironmentsConfiguration(array $configuration)
    {
        if ($this->isValidEnvironmentsConfig($configuration)) {
            $this->environments = $configuration;
            return $this;
        }
        throw new \Exception('Invalid configuration');
    }

    /**
     * @inheritdoc
     */
    public function getEnvironmentData()
    {
        if ($this->isValidEnvironmentsConfig($this->environments)) {
            return $this->environments['environments'][$this->getEnvironmentActive()];
        }
        return array();
    }

    /**
     * @inheritdoc
     */
    public function getEnvironmentActive()
    {
        if ($this->isValidEnvironmentsConfig($this->environments)) {
            return $this->environments['environmentActive'];
        }
        return '';
    }

    /**
     * @inheritdoc
     */
    public function isProd()
    {
        $active = $this->getEnvironmentActive();
        return $active == 'PROD';
    }

    /**
     * @inheritdoc
     */
    public function isDev()
    {
        return !$this->isProd();
    }

    /**
     * @@inheritdoc
     */
    public function getVarEnvironmentActive($varName, $default = null)
    {
        $environmentData = $this->getEnvironmentData();
        if (isset($environmentData[$varName]))
            return $environmentData[$varName];
        return $default;
    }

    /**
     * @param array $configuration
     * @return bool
     */
    protected function isValidEnvironmentsConfig(array $configuration)
    {
        return (is_array($configuration) && isset($configuration['environmentActive']) &&
            isset($configuration['environments']) && is_array($configuration['environments']) &&
            isset($configuration['environments'][$configuration['environmentActive']]));
    }
}