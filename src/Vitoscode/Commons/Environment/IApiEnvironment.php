<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 4/26/2019
 * Time: 11:11 AM
 */

namespace Vitoscode\Commons\Environment;

interface IApiEnvironment
{
    /**
     * Set all environments data
     * @param array $configuration
     * @return $this
     */
    function setEnvironmentsConfiguration(array $configuration);

    /**
     * Get data of the environment active
     * @return array
     */
    function getEnvironmentData();

    /**
     * Return the environment active
     * @return string
     */
    function getEnvironmentActive();

    /**
     * Return true if the environment active is PROD
     * @return boolean
     */
    function isProd();

    /**
     * Return true if the environment active is different of PROD
     * @return boolean
     */
    function isDev();

    /**
     * Return the value of var name on the active environment
     * @param $varName
     * @param $default
     * @return mixed
     */
    function getVarEnvironmentActive($varName, $default = null);
}