<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/22/2019
 * Time: 11:15 AM
 */

namespace Vitoscode\Commons\Event;


use Vitoscode\Commons\IApiClient;

class AfterSendApiEvent extends BeforeSendApiEvent
{
    /**
     * @var mixed
     */
    public $response;
}