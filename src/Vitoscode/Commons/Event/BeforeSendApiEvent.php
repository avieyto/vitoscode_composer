<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/22/2019
 * Time: 11:10 AM
 */

namespace Vitoscode\Commons\Event;


use Symfony\Component\EventDispatcher\Event;
use Vitoscode\Commons\IApiClient;

class BeforeSendApiEvent extends Event
{
    /**
     * @var IApiClient $apiInstance
     */
    public $apiInstance;

    /**
     * @var string $url
     */
    public $url;

    /**
     * @var string $requestType
     */
    public $requestType;

    /**
     * @var string $methodName
     */
    public $methodName;

    /**
     * @var mixed
     */
    public $request;

    /**
     * @var mixed
     */
    public $context;
}