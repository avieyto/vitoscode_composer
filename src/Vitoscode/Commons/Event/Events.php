<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/22/2019
 * Time: 11:09 AM
 */

namespace Vitoscode\Commons\Event;


class Events
{
    CONST API_BEFORE_SEND = 'api_before_send';
    CONST API_AFTER_SEND = 'api_after_send';
}