<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/22/2019
 * Time: 11:29 AM
 */

namespace Vitoscode\Commons\Dispatchable;


use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

interface IDispatchableInterface
{
    /**
     * @param EventDispatcherInterface $dispatcher
     * @return void
     */
    function setDispatcher(EventDispatcherInterface $dispatcher);

    /**
     * @return EventDispatcherInterface
     */
    function getDispatcher();

    /**
     * @param string $eventName
     * @param Event $eventObject
     * @return mixed
     */
    function dispatchEvent($eventName, Event $eventObject);
}