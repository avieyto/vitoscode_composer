<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 2/19/2019
 * Time: 2:30 PM
 */

namespace Vitoscode\Commons;


use GuzzleHttp\Client;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Vitoscode\Commons\Environment\IApiEnvironment;
use Vitoscode\Commons\Event\AfterSendApiEvent;
use Vitoscode\Commons\Event\BeforeSendApiEvent;
use Vitoscode\Commons\Event\Events;

abstract class BaseRestApiClient implements IApiClient
{
    /**
     * @var EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    /**
     * @var string $apiName
     */
    protected $apiName;

    /**
     * @var array $config
     */
    protected $config;

    /**
     * @var IApiEnvironment $apiEnvironments
     */
    protected $apiEnvironments;

    /**
     * @var Client $requestClient
     */
    protected $requestClient;

    /**
     * @inheritdoc
     */
    public function __construct($apiName, IApiEnvironment $apiEnvironments)
    {
        $this->apiName = $apiName;
        if ($apiEnvironments)
            $this->setApiEnvironments($apiEnvironments);

        $this->requestClient = new Client();
    }

    /**
     * @inheritdoc
     */
    public function getDispatcher()
    {
        return $this->dispatcher;
    }

    /**
     * @inheritdoc
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getApiEnvironments()
    {
        return $this->apiEnvironments;
    }

    /**
     * @inheritdoc
     */
    public function setApiEnvironments(IApiEnvironment $apiEnvironments)
    {
        $this->apiEnvironments = $apiEnvironments;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getApiName()
    {
        return $this->apiName;
    }

    /**
     * @inheritdoc
     */
    public function dispatchEvent($eventName, Event $eventObject)
    {
        if ($this->dispatcher)
            $this->dispatcher->dispatch($eventName, $eventObject);
    }

    /**
     * @inheritdoc
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($methodName, $requestType, $url = '', array $options = [], $context = null)
    {
        $eventBeforeSendApi = new BeforeSendApiEvent();
        $eventBeforeSendApi->apiInstance = $this;
        $eventBeforeSendApi->methodName = $methodName;
        $eventBeforeSendApi->request = $options;
        $eventBeforeSendApi->requestType = $requestType;
        $eventBeforeSendApi->url = $url;
        $eventBeforeSendApi->context = $context;
        $this->dispatchEvent(Events::API_BEFORE_SEND, $eventBeforeSendApi);
        $requestType = $eventBeforeSendApi->requestType;
        $url = $eventBeforeSendApi->url;
        $options = $eventBeforeSendApi->request;
        $response = $this->requestClient->request($requestType, $url, $options);
        $evenAfterSendApi = new AfterSendApiEvent();
        $evenAfterSendApi->apiInstance = $this;
        $evenAfterSendApi->methodName = $methodName;
        $evenAfterSendApi->request = $options;
        $evenAfterSendApi->requestType = $requestType;
        $evenAfterSendApi->url = $url;
        $evenAfterSendApi->context = $context;
        $evenAfterSendApi->response = $response;
        $this->dispatchEvent(Events::API_AFTER_SEND, $evenAfterSendApi);
        return $evenAfterSendApi->response;
    }
}
