<?php
/**
 * Created by PhpStorm.
 * User: avieyto
 * Date: 7/6/2017
 * Time: 11:17 PM
 */

namespace Vitoscode\FirstDataApi\Response;

class FirstDataAbstractResponse
{
    /**
     * @var float $amount
     */
    public $amount;

    /**
     * @var string $authorization_num
     */
    public $authorization_num;

    /**
     * @var string $avs
     */
    public $avs;

    /**
     * @var string $bank_message
     */
    public $bank_message;

    /**
     * @var string $bank_resp_code
     */
    public $bank_resp_code;

    /**
     * @var string $cardholder_name
     */
    public $cardholder_name;

    /**
     * @var string $cc_expiry
     */
    public $cc_expiry;

    /**
     * @var string $cc_number
     */
    public $cc_number;

    /**
     * @var string $cc_verification_str1
     */
    public $cc_verification_str1;

    /**
     * @var string $cc_verification_str2
     */
    public $cc_verification_str2;

    /**
     * @var string $client_email
     */
    public $client_email;

    /**
     * @var string $client_ip
     */
    public $client_ip;

    /**
     * @var string $credit_card_type
     */
    public $credit_card_type;

    /**
     * @var string $ctr
     */
    public $ctr;

    /**
     * @var string $currency_code
     */
    public $currency_code;

    /**
     * @var integer $cvd_presence_ind
     */
    public $cvd_presence_ind;

    /**
     * @var string $cvv2
     */
    public $cvv2;

    /**
     * @var string $exact_message
     */
    public $exact_message;

    /**
     * @var string $exact_resp_code
     */
    public $exact_resp_code;

    /**
     * @var string $gateway_id
     */
    public $gateway_id;

    /**
     * @var string $merchant_address
     */
    public $merchant_address;

    /**
     * @var string $merchant_city
     */
    public $merchant_city;

    /**
     * @var string $merchant_country
     */
    public $merchant_country;

    /**
     * @var string $merchant_name
     */
    public $merchant_name;

    /**
     * @var string $merchant_postal
     */
    public $merchant_postal;

    /**
     * @var string $merchant_province
     */
    public $merchant_province;

    /**
     * @var string $merchant_url
     */
    public $merchant_url;

    /**
     * @var string $partial_redemption
     */
    public $partial_redemption;

    /**
     * @var string $reference_no
     */
    public $reference_no;

    /**
     * @var string $sequence_no
     */
    public $sequence_no;

    /**
     * @var string $transaction_approved
     */
    public $transaction_approved;

    /**
     * @var string $transaction_error
     */
    public $transaction_error;

    /**
     * @var string $transaction_tag
     */
    public $transaction_tag;

    /**
     * @var string $transaction_type
     */
    public $transaction_type;

    /**
     * @var string $zip_code
     */
    public $zip_code;
}