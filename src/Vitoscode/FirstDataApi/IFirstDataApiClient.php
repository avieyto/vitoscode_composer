<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 6/5/2019
 * Time: 11:28 AM
 */

namespace Vitoscode\FirstDataApi;


use Vitoscode\Commons\IApiClient;
use Vitoscode\FirstDataApi\Exception\IFirstDataException;
use Vitoscode\FirstDataApi\Request\CompleteAuthorizationRequest;
use Vitoscode\FirstDataApi\Request\PreAuthorizationRequest;
use Vitoscode\FirstDataApi\Request\PurchaseRequest;
use Vitoscode\FirstDataApi\Request\RefundRequest;
use Vitoscode\FirstDataApi\Request\VoidAuthorizationRequest;
use Vitoscode\FirstDataApi\Response\CompleteAuthorizationResponse;
use Vitoscode\FirstDataApi\Response\PreAuthorizationResponse;
use Vitoscode\FirstDataApi\Response\PurchaseResponse;
use Vitoscode\FirstDataApi\Response\RefundResponse;
use Vitoscode\FirstDataApi\Response\VoidAuthorizationResponse;

interface IFirstDataApiClient extends IApiClient
{
    /**
     * Make a purchase transaction to the first data merchant api.
     * @param PurchaseRequest $purchaseRequest
     * @return PurchaseResponse
     * @throws IFirstDataException
     */
    function Purchase(PurchaseRequest $purchaseRequest);

    /**
     * Perform a pre authorization to the first data merchant api.
     * @param PreAuthorizationRequest $preAuthorizationRequest
     * @return PreAuthorizationResponse
     * @throws IFirstDataException
     */
    function PreAuthorization(PreAuthorizationRequest $preAuthorizationRequest);

    /**
     * Perform a complete authorization to the first data merchant api.
     * @param CompleteAuthorizationRequest $completeAuthorizationRequest
     * @return CompleteAuthorizationResponse
     * @throws IFirstDataException
     */
    function CompleteAuthorization(CompleteAuthorizationRequest $completeAuthorizationRequest);

    /**
     * Perform a complete authorization to the first data merchant api.
     * @param VoidAuthorizationRequest $voidAuthorizationRequest
     * @return VoidAuthorizationResponse
     * @throws IFirstDataException
     */
    function VoidAuthorization(VoidAuthorizationRequest $voidAuthorizationRequest);

    /**
     * Perform a refund authorization to the first data merchant api.
     * @param RefundRequest $refundRequest
     * @return RefundResponse
     * @throws IFirstDataException
     */
    function Refund(RefundRequest $refundRequest);
}
