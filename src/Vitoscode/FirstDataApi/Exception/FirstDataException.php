<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/15/2019
 * Time: 11:03 AM
 */

namespace Vitoscode\FirstDataApi\Exception;


class FirstDataException extends \Exception implements IFirstDataException
{
    /**
     * @var string $firstDataResponse
     */
    public $firstDataResponse;

    public function __construct($message = "", $code = 0, \Throwable $previous = null, $firstDataResponse = null)
    {
        parent::__construct($message, $code, $previous);
        $this->firstDataResponse = $firstDataResponse;
    }
}
