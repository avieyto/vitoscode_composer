<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 6/6/2019
 * Time: 2:30 PM
 */

namespace Vitoscode\FirstDataApi;


use Vitoscode\Commons\BaseRestApiClient;
use Vitoscode\Commons\Environment\IApiEnvironment;
use Vitoscode\Decoder\JsonDecoder;
use Vitoscode\Encoder\EncoderCleanJson;
use Vitoscode\FirstDataApi\Authorization\GenerateAuthorization;
use Vitoscode\FirstDataApi\Authorization\IGenerateAuthorization;
use Vitoscode\FirstDataApi\Exception\FirstDataException;
use Vitoscode\FirstDataApi\Request\CompleteAuthorizationRequest;
use Vitoscode\FirstDataApi\Request\FirstDataAbstractRequest;
use Vitoscode\FirstDataApi\Request\PreAuthorizationRequest;
use Vitoscode\FirstDataApi\Request\PurchaseRequest;
use Vitoscode\FirstDataApi\Request\RefundRequest;
use Vitoscode\FirstDataApi\Request\VoidAuthorizationRequest;
use Vitoscode\FirstDataApi\Response\FirstDataAbstractResponse;
use GuzzleHttp\Exception\GuzzleException;

class FirstDataApiClient extends BaseRestApiClient implements IFirstDataApiClient
{
    /**
     * @var IFirstDataApiEnvironment $apiEnvironments
     */
    protected $apiEnvironments;

    /**
     * @var IGenerateAuthorization $firstDataAuthorizationHeaderGenerateService
     */
    protected $firstDataAuthorizationHeaderGenerateService;

    public function __construct($apiName, IApiEnvironment $apiEnvironments, IGenerateAuthorization $firstDataAuthorizationHeaderGenerateService = null)
    {
        if (!($apiEnvironments instanceof IFirstDataApiEnvironment)) {
            throw new \Exception("Invalid First Data Api Environment, apiEnvironments must be a class than implement IFirstDataApiEnvironment");
        }
        if (!$firstDataAuthorizationHeaderGenerateService)
            $this->firstDataAuthorizationHeaderGenerateService = new GenerateAuthorization();
        else
            $this->firstDataAuthorizationHeaderGenerateService = $firstDataAuthorizationHeaderGenerateService;
        parent::__construct($apiName, $apiEnvironments);
    }

    /**
     * @inheritdoc
     */
    public function Purchase(PurchaseRequest $request)
    {
        $responseClass = '\Vitoscode\FirstDataApi\Response\PurchaseResponse';
        return $this->sendRequest($request, 'PURCHASE', $responseClass, null);
    }

    /**
     * @inheritdoc
     */
    public function PreAuthorization(PreAuthorizationRequest $request)
    {
        $responseClass = '\Vitoscode\FirstDataApi\Response\PreAuthorizationResponse';
        return $this->sendRequest($request, 'PRE-AUTHORIZATION', $responseClass, null);
    }

    /**
     * @inheritdoc
     */
    public function CompleteAuthorization(CompleteAuthorizationRequest $request)
    {
        $responseClass = '\Vitoscode\FirstDataApi\Response\CompleteAuthorizationResponse';
        return $this->sendRequest($request, 'COMPLETE-AUTHORIZATION', $responseClass, null);
    }

    /**
     * @inheritdoc
     */
    public function VoidAuthorization(VoidAuthorizationRequest $request)
    {
        $responseClass = '\Vitoscode\FirstDataApi\Response\VoidAuthorizationResponse';
        return $this->sendRequest($request, 'VOID-AUTHORIZATION', $responseClass, null);
    }

    /**
     * @inheritdoc
     */
    public function Refund(RefundRequest $request)
    {
        $responseClass = '\Vitoscode\FirstDataApi\Response\RefundResponse';
        return $this->sendRequest($request, 'REFUND', $responseClass, null);
    }

    /**
     * @param FirstDataAbstractRequest $request
     * @param $methodName
     * @param $responseClass
     * @param $context
     * @return FirstDataAbstractResponse
     * @throws FirstDataException
     */
    protected function sendRequest(FirstDataAbstractRequest $request, $methodName, $responseClass, $context)
    {
        /**
         * @var $apiEnvironment IFirstDataApiEnvironment
         */
        $apiEnvironment = $this->getApiEnvironments();
        $request->gateway_id = $apiEnvironment->getGatewayId();
        $request->password = $apiEnvironment->getGatewayPassword();
        $url = $apiEnvironment->getUrl();
        $authorizationHeader = $this->firstDataAuthorizationHeaderGenerateService->generate($apiEnvironment, $request);
        $post = EncoderCleanJson::encodeCleanToJson($request);

        $options = [
            'headers' => [
                'User-Agent' => 'PHP First Data Api Client/2.0',
                'Accept-Languages' => 'en_US',
                'Content-Type' => $authorizationHeader->getContentType(),
                'Accept' => 'application/json',
                'Content-Length' => strlen($post),
                'Authorization' => $authorizationHeader->getAuthorization(),
                'X-GGe4-Content-SHA1' => $authorizationHeader->getXGGe4ContentDigest(),
                'X-GGe4-Date' => $authorizationHeader->getXGGe4Date()
            ],
            'verify' => false,
            'body' => $post
        ];


        try {
            $guzzleResponse = $this->request($methodName, "POST", $url, $options, $context);
            if ($guzzleResponse) {
                $statusCode = $guzzleResponse->getStatusCode();
                $reasonPhrase = $guzzleResponse->getReasonPhrase();
                $responseString = $guzzleResponse->getBody()->getContents();
                if ($statusCode != 200)
                    throw new FirstDataException('An error occurred requesting paypal ' . $methodName . ', Reason: ' . $reasonPhrase, $statusCode, null, $responseString);
                $jsonObject = json_decode($responseString);
                if (!$jsonObject)
                    throw new FirstDataException('Invalid response returned by paypal on ' . $methodName, 0, null, $responseString);

                /**
                 * @var $response FirstDataAbstractResponse
                 */
                $response = JsonDecoder::JsonObject2Object($responseClass, $jsonObject);
                return $response;
            } else
                throw new FirstDataException('An error occurred requesting paypal ' . $methodName, 0, null, $guzzleResponse);
        }
        catch(GuzzleException $exception) {
            throw new FirstDataException($exception->getMessage(), $exception->getCode(), $exception, null);
        }
    }
}
