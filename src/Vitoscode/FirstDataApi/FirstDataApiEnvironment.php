<?php
/**
 * Created by PhpStorm.
 * User: aviey
 * Date: 6/19/2019
 * Time: 8:44 PM
 */

namespace Vitoscode\FirstDataApi;


use Vitoscode\Commons\Environment\ApiEnvironment;
use Vitoscode\Commons\Environment\IApiEnvironment;

class FirstDataApiEnvironment extends ApiEnvironment implements IFirstDataApiEnvironment
{

    /**
     * @return string
     */
    public function getGatewayId()
    {
        return $this->getVarEnvironmentActive('gatewayId', null);
    }

    /**
     * @return string
     */
    public function getGatewayPassword()
    {
        return $this->getVarEnvironmentActive('gatewayPassword', null);
    }

    /**
     * @return string
     */
    public function getKeyId()
    {
        return $this->getVarEnvironmentActive('keyId', null);
    }

    /**
     * @return string
     */
    public function getHmacKey()
    {
        return $this->getVarEnvironmentActive('hmacKey', null);
    }

    /**
     * @return string
     */
    public function getApiVersion()
    {
        return $this->getVarEnvironmentActive('apiVersion', null);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->getVarEnvironmentActive('url', '');
    }
}