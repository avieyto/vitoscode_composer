<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 6/6/2019
 * Time: 12:16 PM
 */

namespace Vitoscode\FirstDataApi\Request;


use Vitoscode\FirstDataApi\FirstDataTransactionTypes;

class PurchaseRequest extends FirstDataAbstractRequest
{
    /**
     * PurchaseRequest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->transaction_type = FirstDataTransactionTypes::TRANSACTION_PURCHASE;
    }
}