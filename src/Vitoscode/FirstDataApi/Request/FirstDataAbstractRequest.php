<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 6/6/2019
 * Time: 12:06 PM
 */

namespace Vitoscode\FirstDataApi\Request;


use Vitoscode\FirstDataApi\FirstDataTransactionTypes;

class FirstDataAbstractRequest
{
    /**
     * @var string $gateway_id
     */
    public $gateway_id;

    /**
     * @var string $password
     */
    public $password;

    /**
     * @var string $transaction_type
     */
    public $transaction_type = FirstDataTransactionTypes::TRANSACTION_PURCHASE;

    /**
     * @var string $cardholder_name
     */
    public $cardholder_name;

    /**
     * @var string $cc_number
     */
    public $cc_number;

    /**
     * Expiration date of the credit card, format MMYY
     * @var string $cc_expiry
     */
    public $cc_expiry;

    /**
     * @var float $amount
     */
    public $amount;

    /**
     * Credit Card Address format street|zip|city|state|country
     * @var string $cc_verification_str1
     */
    public $cc_verification_str1;

    /**
     * Credit Card CVV
     * @var string $cc_verification_str2
     */
    public $cc_verification_str2;

    /**
     * @var string $reference_no
     */
    public $reference_no;

    /**
     * @var string $zip_code
     */
    public $zip_code;

    /**
     * @var string $client_ip
     */
    public $client_ip;

    /**
     * @var string $client_email
     */
    public $client_email;

    /**
     * @var string $cvd_presence_ind
     */
    public $cvd_presence_ind;

    /**
     * @var string $language
     */
    public $language;

    /**
     * FirstDataAbstractRequest constructor.
     */
    public function __construct()
    {

    }
}