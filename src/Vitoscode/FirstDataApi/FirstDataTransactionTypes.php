<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 6/6/2019
 * Time: 12:08 PM
 */

namespace Vitoscode\FirstDataApi;


class FirstDataTransactionTypes
{
    const TRANSACTION_PURCHASE = '00';
    const TRANSACTION_PRE_AUTH = '01';
    const TRANSACTION_PRE_AUTH_COMPLETE = '02';
    const TRANSACTION_FORCED_POST = '03';
    const TRANSACTION_REFUND = '04';
    const TRANSACTION_PRE_AUTH_ONLY = '05';
    const TRANSACTION_PAYPAL_ORDER = '07';
    const TRANSACTION_VOID = '13';
    const TRANSACTION_TAGGED_PRE_AUTH_COMPLETE = '32';
    const TRANSACTION_TAGGED_VOID = '33';
    const TRANSACTION_TAGGED_REFUND = '34';
    const TRANSACTION_CASH_OUT = '83';
    const TRANSACTION_ACTIVATION = '85';
    const TRANSACTION_BALANCE_INQUIRY = '86';
    const TRANSACTION_RELOAD = '88';
    const TRANSACTION_DEACTIVATION = '89';
}