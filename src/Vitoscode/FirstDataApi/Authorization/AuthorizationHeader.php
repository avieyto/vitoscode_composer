<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/15/2019
 * Time: 10:13 AM
 */

namespace Vitoscode\FirstDataApi\Authorization;


class AuthorizationHeader implements IAuthorizationHeader
{
    /**
     * @var string $authorization
     */
    private $authorization;

    /**
     * @var string $digestContent
     */
    private $digestContent;

    /**
     * @var \DateTime $dateTime
     */
    private $dateTime;

    /**
     * @var string $contentType
     */
    private $contentType;

    public function __construct($authorization, $digestContent, $dateTime, $contentType = 'application/json')
    {
        $this->authorization = $authorization;
        $this->digestContent = $digestContent;
        $this->dateTime = $dateTime;
        $this->contentType = $contentType;
    }

    /**
     * @inheritdoc
     */
    public function getAuthorization()
    {
        return $this->authorization;
    }

    /**
     * @inheritdoc
     */
    public function getXGGe4ContentDigest()
    {
        return $this->digestContent;
    }

    /**
     * @inheritdoc
     */
    public function getXGGe4Date()
    {
        return $this->dateTime->format('Y-m-d').'T'.$this->dateTime->format('H:i:s').'Z';
    }

    /**
     * @inheritdoc
     */
    public function getContentType()
    {
        return $this->contentType;
    }
}
