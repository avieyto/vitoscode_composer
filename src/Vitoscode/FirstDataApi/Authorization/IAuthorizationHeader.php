<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/15/2019
 * Time: 10:07 AM
 */

namespace Vitoscode\FirstDataApi\Authorization;


interface IAuthorizationHeader
{
    /**
     * return the authorization with the signature to be used in the header of the first data api
     * @return string
     */
    function getAuthorization();

    /**
     * return the content digest encoded with SHA1 used in the authorization creation to be used in the header of the first data api
     * @return string
     */
    function getXGGe4ContentDigest();

    /**
     * return an date string format Y-m-dTH:i:sZ to be used on the header of the first data api
     * @return string
     */
    function getXGGe4Date();

    /**
     * return content type used to generate the authorization signature
     * @return string
     */
    function getContentType();
}
