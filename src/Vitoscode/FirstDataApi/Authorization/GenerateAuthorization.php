<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/15/2019
 * Time: 10:20 AM
 */

namespace Vitoscode\FirstDataApi\Authorization;


use Vitoscode\Encoder\EncoderCleanJson;
use Vitoscode\FirstDataApi\FirstDataApiEnvironment;
use Vitoscode\FirstDataApi\IFirstDataApiEnvironment;
use Vitoscode\FirstDataApi\Request\FirstDataAbstractRequest;
use Vitoscode\Util\DateTime\DateTimeHelper;

class GenerateAuthorization implements IGenerateAuthorization
{
    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function generate(IFirstDataApiEnvironment $apiEnvironment, FirstDataAbstractRequest $request)
    {
        $contentType = 'application/json';
        $url = $apiEnvironment->getUrl();
        $datetime = DateTimeHelper::getDateTime();
        $post = EncoderCleanJson::encodeCleanToJson($request);
        $contentDigest = sha1($post);
        $hashtime = DateTimeHelper::getHashTime($datetime);
        $signatureRaw = "POST\n$contentType\n$contentDigest\n$hashtime\n$url";
        $signature = base64_encode(hash_hmac('sha1', $signatureRaw, $apiEnvironment->getHmacKey(), TRUE));
        $authorization = 'GGE4_API '.$apiEnvironment->getKeyId().':'.$signature;
        return new AuthorizationHeader($authorization, $contentDigest, $datetime, $contentType);
    }
}
