<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 8/15/2019
 * Time: 10:20 AM
 */

namespace Vitoscode\FirstDataApi\Authorization;

use Vitoscode\FirstDataApi\IFirstDataApiEnvironment;
use Vitoscode\FirstDataApi\Request\FirstDataAbstractRequest;

interface IGenerateAuthorization
{
    /**
     * Generate a new IAuthorizationHeader from the request
     * @param IFirstDataApiEnvironment $apiEnvironment
     * @param FirstDataAbstractRequest $request
     * @return IAuthorizationHeader
     */
    function generate(IFirstDataApiEnvironment $apiEnvironment, FirstDataAbstractRequest $request);
}
