<?php
/**
 * Created by PhpStorm.
 * User: av10064
 * Date: 6/5/2019
 * Time: 11:29 AM
 */

namespace Vitoscode\FirstDataApi;


use Vitoscode\Commons\Environment\IApiEnvironment;

interface IFirstDataApiEnvironment extends IApiEnvironment
{
    /**
     * @return string
     */
    function getGatewayId();

    /**
     * @return string
     */
    function getGatewayPassword();

    /**
     * @return string
     */
    function getKeyId();

    /**
     * @return string
     */
    function getHmacKey();

    /**
     * @return string
     */
    function getApiVersion();

    /**
     * @return string
     */
    function getUrl();
}